﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Graph.NET.Utils;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Threading;
using Graph.NET;

namespace Graph.NET_Tests
{
    [TestClass]
    public class UnitTests
    {
        private SvgExporter m_exporter;

        [TestInitialize]
        public void SetUp()
        {
            var path = Path.GetFullPath(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, @"..\..\TestData\data_small.gv"));

            m_exporter = new SvgExporter(File.ReadAllText(path), true);
        }

        [TestMethod]
        public void TestAsync()
        {
            CancellationTokenSource source = new CancellationTokenSource();

            var task = m_exporter.CreateSvgTypeUsingDotExecutableAsync(source.Token);
            
            try
            {
                while (!task.IsCompleted)
                {
                    Thread.Sleep(500);
                }

                var result = task.Result;
            }
            catch (AggregateException e)
            {
                Assert.Fail(String.Format("Exception thrown: {0}", e.Message));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(OperationCanceledException))]
        public void TestAsyncCancellation()
        {
            var source = new CancellationTokenSource();

            var task = m_exporter.CreateSvgTypeUsingDotExecutableAsync(source.Token);

            try
            {
                while (!task.IsCompleted)
                {
                    Thread.Sleep(20);

                    source.Cancel();
                }

                Assert.Fail("Task was cancelled but exception not thrown.");
            }
            catch (AggregateException ae)
            {
                var squish = ae.Flatten();

                // Mark all exceptions as handled.
                squish.Handle(ex =>
                {
                    if (ex is OperationCanceledException)
                    {
                        throw ex;
                    }

                    return true;
                });
            }
        }

        [TestMethod]
        public void TestSvgToString()
        {
            CancellationTokenSource source = new CancellationTokenSource();

            var task = m_exporter.CreateSvgTypeUsingDotExecutableAsync(source.Token);

            try
            {
                while (!task.IsCompleted)
                {
                    Thread.Sleep(500);
                }

                var result = task.Result;

                result.Init(AppDomain.CurrentDomain.BaseDirectory);
            }
            catch (AggregateException e)
            {
                Assert.Fail(String.Format("Exception thrown: {0}", e.Message));
            }
        }
    }
}
