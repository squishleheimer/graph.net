﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Graph.NET.Utils;

namespace Graph.NET
{
    public class HighlightBox : AbstractDrawable
    {
        private static readonly float DEFAULT_CORNER_SIZE = 5f;

        private float m_cornerSize = DEFAULT_CORNER_SIZE;

        /// <summary>
        /// Animation variable to make the highlight box pulsate.
        /// </summary>
        private float m_swell = 0f;

        /// <summary>
        /// Tracking animation time.
        /// </summary>
        private double m_timePassed = 0;

        /// <summary>
        /// Node that the box is highlighting.
        /// <remarks>Should never be null.</remarks>
        /// </summary>
        private IGroupNode m_node = null;

        public IGroupNode Node
        {
            get { return m_node; }
            set { m_node = value; }
        }

        /// <summary>
        /// Animates the corners.
        /// </summary>
        /// <param name="deltaTimeInSeconds">The seconds passed since last tick.</param>
        public override void Animate(double deltaTimeInSeconds)
        {
            m_timePassed += deltaTimeInSeconds;

            m_swell += (float)Math.Cos(m_timePassed * 2.0 * Math.PI * 2.0);
            m_swell *= 0.04f;

            m_cornerSize += 1.0f + 0.5f * (float)Math.Cos(-m_timePassed * 3.0 * Math.PI);
            m_cornerSize *= 0.8f;
        }

        public void Reset()
        {
            m_swell = 0;
            m_cornerSize = DEFAULT_CORNER_SIZE;
            m_timePassed = 0;
            m_node = null;
        }

        /// <summary>
        /// Draws the HighlightBox.
        /// </summary>
        /// <param name="g">Graphics object required to perform drawing.</param>
        /// <param name="m">Transform from the panel.</param>
        public void Draw(Graphics g, Matrix m)
        {
            if (m_node != null && Visible)
            {
                PointF[] points = m_node.GetBoundCorners();

                m.Multiply(m_node.HeirarchyTransform);

                m.TransformPoints(points);

                // Grab the transformed centre.
                PointF centre = MathUtils.GetRectangleCentre(
                    ConversionUtils.RectangleFFromPoints(points));

                Vector a = Vector.FromPointF(centre);

                // Calculate the "swollen" corner points.
                for (uint i = 0; i < points.Length; ++i)
                {   
                    Vector b = Vector.FromPointF(points[i]);

                    Vector ab = b - a;  // - Create vector from rectangle centre to corner.
                    ab *= m_swell;      // - Expand the vector based on animated factor.
                    ab = b + ab;        // - Calculate end point.

                    points[i] = ab.ToPointF();
                }

                GraphicsContainer c = g.BeginContainer();
                {
                    g.MultiplyTransform(Transform);

                    using (Pen pen = new Pen(Brushes.Red, Thickness))
                    {
                        g.DrawLines(pen, new PointF[] {
                        new PointF(points[0].X + m_cornerSize, points[0].Y),
                        points[0],
                        new PointF(points[0].X, points[0].Y + m_cornerSize)});

                        g.DrawLines(pen, new PointF[] {
                        new PointF(points[1].X - m_cornerSize, points[1].Y),
                        points[1],
                        new PointF(points[1].X, points[1].Y + m_cornerSize)});

                        g.DrawLines(pen, new PointF[] {
                        new PointF(points[2].X - m_cornerSize, points[2].Y),
                        points[2],
                        new PointF(points[2].X, points[2].Y - m_cornerSize)});

                        g.DrawLines(pen, new PointF[] {
                        new PointF(points[3].X + m_cornerSize, points[3].Y),
                        points[3],
                        new PointF(points[3].X, points[3].Y - m_cornerSize)});
                    }

                } g.EndContainer(c);
            }
        }
    }
}
