using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using Graph.NET;
using System.Xml.Serialization;

namespace Graph.NET
{
    public delegate void TickHandler(double deltaTimeinSeconds);

    public abstract class AbstractDrawable
    {
        public event TickHandler OnAnimationTick = null;
        public event TickHandler OnUpdateTick = null;

        #region Fields
        protected RectangleF m_rectangle;
        protected Matrix m_transform;
        #endregion

        #region Properties
        [XmlIgnore]
        public bool Visible { get; set; }

        [XmlIgnore]
        public Color FillColor { get; set; }

        [XmlIgnore]
        public bool Filled { get; set; }

        [XmlIgnore]
        public Color LineColor { get; set; }

        [XmlIgnore]
        public uint Thickness { get; set; }

        [XmlIgnore]
        public Single[] DashPattern { get; set; }

        [XmlIgnore]
        public RectangleF Rect
        {
            get { return m_rectangle; }
            set { m_rectangle = value; }
        }

        [XmlIgnore]
        public Matrix Transform
        {
            get { return m_transform; }
            set { m_transform = value; }
        }

        #endregion

        protected AbstractDrawable()
        {
            LineColor = Color.Black;
            Thickness = 1;
            Filled = false;
            Visible = true;

            m_rectangle = RectangleF.Empty;
            m_transform = new Matrix();
        }

        public virtual void Animate(double deltaTimeInSeconds)
        {
            if (OnAnimationTick != null)
            {
                OnAnimationTick(deltaTimeInSeconds);
            }
        }

        public virtual void Update(double deltaTimeInSeconds)
        {
            if (OnUpdateTick != null)
            {
                OnUpdateTick(deltaTimeInSeconds);
            }
        }
    }
}
