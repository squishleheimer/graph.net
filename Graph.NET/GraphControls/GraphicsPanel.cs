using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;
using Graph.NET.Utils;
using System.Threading;

namespace Graph.NET
{
    namespace GraphControls
    {
        public partial class GraphicsPanel : Panel
        {
            public static readonly SizeF UNIT_SIZE = new Size(1, 1);

            public event PaintEventHandler PrePaint = delegate { };
            public event PaintEventHandler PostPaint = delegate { };

            public event EventHandler OnScaleUpdate = delegate { };

            #region Fields

            private ISvgType m_svg;
            private ToolTip m_toolTip;

            private PointF m_translation = PointF.Empty;
            private SizeF m_scale = UNIT_SIZE;
            private float m_rotation = 0f;

            private IGroupNode[] m_highlightedNodes = null;

            private bool m_showTraversalHighlight = true;
            private bool m_showTooltips = false;
            private string m_toolTipText = String.Empty;
            private string m_toolTipTitle = String.Empty;

            /// <summary>
            /// Who what controls the control points.
            /// </summary>
            private ControlPointManager m_controlPointMgr = new ControlPointManager();

            /// <summary>
            /// An animated highlight box to make things look a little
            /// special and potentially cause something bad to happen.
            /// </summary>
            private HighlightBox m_highlightBox = new HighlightBox();

            /// <summary>
            /// Feedback for transition adding and editing.
            /// </summary>
            private EditArrow m_editArrow = new EditArrow()
            {
                Thickness = 2,
                FillColor = Color.Black,
                LineColor = Color.Black,
                Rect = new RectangleF(PointF.Empty, new SizeF(40, 15))
            };

            /// <summary>
            /// Stamp at last timer tick.
            /// </summary>
            DateTime m_lastAnimationTick = DateTime.Now;

            /// <summary>
            /// Stamp at last timer tick.
            /// </summary>
            DateTime m_lastUpdateTick = DateTime.Now;

            /// <summary>
            /// Timer for animations. Must have a small interval.
            /// </summary>
            private System.Timers.Timer m_animationTimer;

            /// <summary>
            /// Timer for updates. Can have a larger interval to 
            /// throttle expensive updates and boost performance.
            /// </summary>
            private System.Timers.Timer m_updateTimer;

            #endregion

            #region Properties

            internal ControlPointManager ControlPointManager
            {
                get { return m_controlPointMgr; }
            }

            /// <summary>
            /// This provides internal means to access the SVG object.
            /// We don't want to allow direct control to external assemblies.
            /// </summary>
            internal ISvgType Svg
            {
                get { return m_svg; }
                set
                {
                    m_svg = value;

                    m_selectedNode = null;
                    m_foreNode = null;

                    if (m_svg != null)
                    {
                        this.Size = Size.Round(m_svg.ViewBox.Size);
                        this.Location = Point.Round(m_svg.ViewBox.Location + new Size(2, 2));
                    }
                    else
                    {
                        this.Size = Size.Empty;
                        this.Location = Point.Empty;
                    }

                    m_controlPointMgr.Clear();

                    Refresh();
                }
            }

            public bool AllowTick { get; private set; }

            public bool SvgLoaded
            {
                get { return m_svg != null; }
            }

            public PointF Translation
            {
                get { return m_translation; }
                set { m_translation = value; }
            }

            public new SizeF Scale
            {
                get { return m_scale; }
                set
                {
                    if (value.Width > 0.01f && value.Height > 0.01f && m_scale != value)
                    {
                        m_scale = value;

                        OnScaleUpdate(this, null);
                    }
                }
            }

            public float Rotation
            {
                get { return m_rotation; }
                set { m_rotation = value; }
            }

            public Matrix Transform
            {
                get
                {
                    Matrix m = new Matrix();

                    m.Rotate(m_rotation, MatrixOrder.Prepend);
                    m.Scale(m_scale.Width, m_scale.Height, MatrixOrder.Prepend);
                    m.Translate(m_translation.X, m_translation.Y, MatrixOrder.Prepend);

                    return m;
                }
            }

            public string ForeNodeTitle
            {
                get
                {
                    if (m_foreNode != null && !String.IsNullOrWhiteSpace(m_foreNode.Title))
                    {
                        return m_foreNode.Title;
                    }

                    return String.Empty;
                }
            }

            public PointF ForeNodeCentre
            {
                get
                {
                    if (m_foreNode != null)
                    {
                        return m_foreNode.GetTransformedCentre(Transform.Clone());
                    }

                    return PointF.Empty;
                }
            }

            public bool ShowTraversalHighlight
            {
                get { return m_showTraversalHighlight; }
                set { m_showTraversalHighlight = value; }
            }

            public bool ShowToolTips
            {
                get { return m_showTooltips; }
                set { m_showTooltips = value; }
            }

            public string ToolTipText
            {
                get { return m_toolTipText; }
                set { m_toolTipText = value; }
            }

            public string ToolTipTitle
            {
                get { return m_toolTipTitle; }
                set { m_toolTipTitle = value; }
            }

            public SizeF SvgSize
            {
                get
                {
                    if (SvgLoaded)
                    {
                        return m_svg.Dimensions;
                    }

                    return SizeF.Empty;
                }
            }

            #endregion

            #region Constructors

            /// <summary>
            /// Default CTOR.
            /// <remarks>The Svg object will be uninitialised.</remarks>
            /// </summary>
            public GraphicsPanel()
            {
                Init();
            }

            /// <summary>
            /// CTOR allowing setting of the Svg.
            /// </summary>
            /// <param name="svg">A populated svg object.</param>
            public GraphicsPanel(ISvgType svg)
            {
                Init();

                Svg = svg;
            }

            #endregion

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    m_animationTimer.Close();
                    m_updateTimer.Close();
                }

                base.Dispose(disposing);
            }

            /// <summary>
            /// Initialization of the Panel.
            /// </summary>
            private void Init()
            {
                this.SuspendLayout();

                this.Name = "GraphicsPanel";
                this.DoubleBuffered = true;

                this.Paint += new PaintEventHandler(this.OnPaint);
                this.PostPaint += new PaintEventHandler(this.OnPostPaint);

                this.MouseEnter += (sender, args) => 
                { 
                    AllowTick = true; 
                    m_highlightBox.Visible = true; 
                };

                this.MouseLeave += (sender, args) =>
                { 
                    AllowTick = false; 
                    m_highlightBox.Visible = false; 
                    this.Invalidate(); 
                };

                this.MouseMove += new MouseEventHandler(this.OnMouseMove);
                this.MouseClick += new MouseEventHandler(this.OnMouseClick);
                this.MouseUp += new MouseEventHandler(this.OnMouseUp);
                this.MouseDown += new MouseEventHandler(this.OnMouseDown);

                this.ControlPointMoveBegin += new ControlPointEditHandler(HandleControlPointMoveBegin);
                this.GroupNodeMouseEnterEvent += new GroupNodeEventHandler(GraphicsPanel_GroupNodeMouseEnterEvent);
                this.GroupNodeMouseExitEvent += new GroupNodeEventHandler(GraphicsPanel_GroupNodeMouseExitEvent);

                m_toolTip = new ToolTip()
                {
                    ShowAlways = false,
                    UseFading = true
                };

                m_animationTimer = new System.Timers.Timer()
                {
                    Enabled = true,
                    Interval = 10,
                    SynchronizingObject = this
                };

                m_animationTimer.Elapsed += new System.Timers.ElapsedEventHandler(AnimationTick);

                m_updateTimer = new System.Timers.Timer()
                {
                    Enabled = true,
                    Interval = 100,
                    SynchronizingObject = this
                };

                m_updateTimer.Elapsed += new System.Timers.ElapsedEventHandler(UpdateTick);
                
                m_editArrow.OnUpdateTick += new TickHandler(m_editArrow_OnUpdateTick);

                AllowTick = false;

                this.ResumeLayout(false);
            }

            private void GraphicsPanel_GroupNodeMouseExitEvent(object sender, GroupNodeEventArgs e)
            {
                if (m_highlightedNodes == null || (!m_highlightedNodes.Any(x => x == e.Node) && m_selectedNode != e.Node))
                {
                    e.Node.Highlighted = false;
                }
            }

            private void GraphicsPanel_GroupNodeMouseEnterEvent(object sender, GroupNodeEventArgs e)
            {
                e.Node.Highlighted = true;
            }

            /// <summary>
            /// Disables all other control points that are related to the one being moved.
            /// </summary>
            /// <param name="controlPoint">The control point that has started moving.</param>
            /// <param name="target"></param>
            private void HandleControlPointMoveBegin(object sender, ControlPointEditEventArgs e)
            {
                foreach (ControlPoint cp in m_controlPointMgr.GetRelatedControlPoints(e.ControlPoint))
                {
                    cp.Visible = false;
                }
            }

            /// <summary>
            /// Handles the animation timer elapsed event.
            /// </summary>
            /// <param name="sender">The sender of the event</param>
            /// <param name="e">The event args.</param>
            private void AnimationTick(object sender, System.Timers.ElapsedEventArgs e)
            {
                if (AllowTick)
                {
                    DateTime signalTime = e.SignalTime;

                    double deltaTimeInSeconds =
                    (double)signalTime.Subtract(m_lastAnimationTick).Milliseconds * 0.001;

                    m_highlightBox.Animate(deltaTimeInSeconds);
                    m_controlPointMgr.Animate(deltaTimeInSeconds);

                    m_editArrow.Animate(deltaTimeInSeconds);
                    m_editArrow.Update(deltaTimeInSeconds);

                    m_lastAnimationTick = signalTime;

                    this.Invalidate(Rectangle.Round(m_highlightBox.Rect));
                }
            }

            /// <summary>
            /// Handles the update timer elapsed event.
            /// </summary>
            /// <param name="sender">The sender of the event</param>
            /// <param name="e">The event args.</param>
            private void UpdateTick(object sender, System.Timers.ElapsedEventArgs e)
            {
                if (SvgLoaded && AllowTick)
                {
                    DateTime signalTime = e.SignalTime;

                    double deltaTimeInSeconds =
                        (double)signalTime.Subtract(m_lastUpdateTick).Milliseconds * 0.001;

                    if (m_foreControlPoint == null)
                    {
                        UpdateForeNode(m_currentMousePos);
                    }

                    m_lastUpdateTick = signalTime;
                }
            }

            /// <summary>
            /// Updates the foreNode based on the mouse location.
            /// </summary>
            /// <param name="location">The current mouse location.</param>
            private void UpdateForeNode(PointF location)
            {
                IGroupNode foreNode = m_svg.FocusNode(GetWorldPosition(location));

                if (m_foreNode != foreNode && m_foreNode != null)
                {
                    SpawnGroupNodeMouseExitEvent(m_foreNode);
                }

                if (foreNode != null)
                {
                    if (m_foreNode != foreNode)
                    {
                        SpawnGroupNodeMouseEnterEvent(foreNode);

                        m_foreNode = foreNode;
                    }

                    if (m_addingTransition)
                    {
                        if (m_foreNode.IsOfClass(ClassType.node))
                        {
                            m_destinationNode = m_foreNode;
                        }
                        else
                        {
                            m_destinationNode = null;
                        }
                    }
                }
                else
                {
                    m_toolTipText = m_toolTipTitle = String.Empty;
                }

                if (foreNode.IsOfClass(ClassType.node))
                {
                    m_highlightBox.Node = foreNode;
                }
                else
                {
                    m_highlightBox.Node = null;
                }

                m_foreNode = foreNode;
            }

            /// <summary>
            /// Updates the foreControlPoint based on the mouse location.
            /// </summary>
            /// <param name="location">The current mouse location.</param>
            private void UpdateForeControlPoint(PointF location)
            {
                ControlPoint foreControlPoint =
                    m_controlPointMgr.IntersectsPoint(location, Transform).FirstOrDefault();

                if (foreControlPoint != null)
                {
                    if (m_foreControlPoint != foreControlPoint)
                    {
                        m_foreControlPoint = foreControlPoint;

                        m_foreControlPoint.Focussed = true;
                    }
                }
                else
                {
                    if (m_foreControlPoint != null)
                    {
                        m_foreControlPoint.Focussed = false;
                    }

                    m_foreControlPoint = null;
                }
            }

            /// <summary>
            /// Converts a point (typically a cursor position)
            /// to the "world" position of the GraphicsPanel.
            /// </summary>
            /// <param name="p">The point to transform.</param>
            /// <returns>The transformed position.</returns>
            private PointF GetWorldPosition(PointF p)
            {
                Matrix m = Transform.Clone();

                m.Invert();

                return MathUtils.TransformPoint(m, p);
            }

            /// <summary>
            /// The painting on the Graphics panel.
            /// </summary>
            private void OnPaint(object sender, PaintEventArgs e)
            {
                Graphics g = e.Graphics;

                if (SvgLoaded)
                {
                    PrePaint(sender, e);

                    GraphicsContainer c = g.BeginContainer();
                    {
                        g.RotateTransform(m_rotation);
                        g.ScaleTransform(m_scale.Width, m_scale.Height);
                        g.TranslateTransform(m_translation.X, m_translation.Y);

                        m_svg.Draw(g);

                    } g.EndContainer(c);
                    
                    PostPaint(sender, e);
                }
            }

            /// <summary>
            /// Drawing over the top of the svg.
            /// </summary>
            private void OnPostPaint(object sender, PaintEventArgs e)
            {
                Matrix m = Transform;
                Graphics g = e.Graphics;

                // Draw a box around the currently selected node.
                DrawSelectionBox(g, m.Clone());
                // Draw the highlight for the foreNode.
                m_highlightBox.Draw(g, m.Clone());
                // Draw the control points.
                m_controlPointMgr.Draw(g, m.Clone());
                m_editArrow.Draw(g);
            }

            /// <summary>
            /// Draws a dotted style rectangle around the currently selected
            /// group node.
            /// </summary>
            /// <param name="g">Graphics required to do drawing.</param>
            /// <param name="m">The matrix to perform any required transformations.</param>
            private void DrawSelectionBox(Graphics g, Matrix m)
            {
                if (m_selectedNode.IsOfClass(ClassType.node | ClassType.cluster | ClassType.graph))
                {
                    PointF[] points = m_selectedNode.GetBoundCorners();

                    m.Multiply(m_selectedNode.HeirarchyTransform);

                    m.TransformPoints(points);

                    using (Pen pen = new Pen(Brushes.Red))
                    {
                        pen.DashStyle = DashStyle.Dot;

                        g.DrawRectangle(pen, Rectangle.Round(
                            ConversionUtils.RectangleFFromPoints(points)));
                    }
                }
            }

            private void UpdateTooltip(PointF p)
            {
                SizeF offset = new SizeF(20, 20);

                Point tooltipPos = Point.Round(PointF.Add(p, offset));

                if (m_showTooltips)
                {
                    m_toolTip.ToolTipTitle = m_toolTipTitle;
                    m_toolTip.Show(m_toolTipText, this, tooltipPos, 3000);
                }
            }

            /// <summary>
            /// Cancels the adding of a transition.
            /// </summary>
            public void CancelEdit()
            {
                m_addingTransition = false;

                m_sourceNode = null;
                m_destinationNode = null;

                m_controlPointMgr.Clear();
                m_editArrow.Reset();

                // Clear custom paint artifacts.
                this.Refresh();
            }

            #region Event Spawners

            private void SpawnGroupNodeSelectionEvent(IGroupNode node)
            {
                if (node != null && GroupNodeSelectionEvent != null)
                {
                    GroupNodeSelectionEvent(this,
                            new GroupNodeEventArgs(node));
                }
            }

            private void SpawnGroupNodeDeselectionEvent(IGroupNode node)
            {
                if (node != null && GroupNodeDeselectionEvent != null)
                {
                    GroupNodeDeselectionEvent(this,
                            new GroupNodeEventArgs(node));
                }
            }

            private void SpawnGroupNodeMouseEnterEvent(IGroupNode node)
            {
                if (node != null && GroupNodeMouseEnterEvent != null)
                {
                    GroupNodeMouseEnterEvent(this, new GroupNodeEventArgs(node));
                }
            }

            private void SpawnGroupNodeMouseExitEvent(IGroupNode node)
            {
                if (node != null && GroupNodeMouseExitEvent != null)
                {
                    GroupNodeMouseExitEvent(this, new GroupNodeEventArgs(node));
                }
            }

            private void SpawnNodeRemovedEvent(IGroupNode node)
            {
                if (GroupNodeRemovedEvent != null)
                {
                    GroupNodeRemovedEvent(this, new GroupNodeEventArgs(node));
                }
            }

            private void SpawnControlPointModeEndEvent(ControlPoint controlPoint, IGroupNode target)
            {
                if (ControlPointMoveEnd != null && controlPoint != null)
                {
                    ControlPointMoveEnd(this, new ControlPointEditEventArgs(controlPoint, target));
                }
            }

            private void SpawnControlPointModeBeginEvent(ControlPoint controlPoint, IGroupNode target)
            {
                if (ControlPointMoveBegin != null && controlPoint != null)
                {
                    ControlPointMoveBegin(this, new ControlPointEditEventArgs(controlPoint, target));
                }
            }

            #endregion

            /// <summary>
            /// Detects whether the mouse cursor is hovering over a graph node.
            /// </summary>
            /// <returns>True if the cursor is over a graph node; false otherwise.</returns>
            public bool IsMouseOverAGraphNode()
            {
                return m_foreNode.IsOfClass(ClassType.node);
            }

            public void SaveGraph()
            {
                if (SvgLoaded)
                {
                    m_svg = svgType.Save(m_svg);
                }
            }

            public void SaveGraphAs(string newPath)
            {
                if (SvgLoaded)
                {
                    svgType.Save(newPath, m_svg);
                }
            }

            /// <summary>
            /// Sets the node that is moused over to be the currently selected node.
            /// </summary>
            private void SelectForeNode()
            {
                if (m_foreControlPoint == null && m_foreNode != null)
                {
                    SetSelectedNode(m_foreNode);

                    SpawnGroupNodeSelectionEvent(m_selectedNode);

                    m_selectedNode.SpawnGroupNodeSelectionEvent(m_selectedNode);
                }
            }

            public void RemoveGroupNode(IGroupNode node)
            {
                if (node != null)
                {
                    SpawnNodeRemovedEvent(node);
                }
            }

            public void RemoveSelectedGraphNode()
            {
                RemoveGroupNode(m_selectedNode);
            }

            private void SetSelectedNode(IGroupNode node)
            {
                // prevent re-selection of the same node.
                if (node == null || m_selectedNode != node)
                {
                    ClearSelectedNode();

                    m_selectedNode = node;

                    m_selectedNode.Highlighted = true;

                    m_controlPointMgr.AddControlPoints(m_selectedNode);

                    if (ShowTraversalHighlight)
                    {
                        List<IGroupNode> nodes = new List<IGroupNode>();

                        GraphUtils.GetAllGroupNodesInTraversal(m_svg, m_selectedNode, nodes);

                        m_highlightedNodes = nodes.ToArray();

                        foreach (IGroupNode gnode in m_highlightedNodes)
                        {
                            gnode.Highlighted = true;
                        }
                    }

                    this.Invalidate();
                }
            }

            /// <summary>
            /// Sets the currently selected control point.
            /// </summary>
            /// <param name="controlPoint">The ControlPoint to set as selected.</param>
            private void SetSelectedControlPoint(ControlPoint controlPoint)
            {
                if (controlPoint != null && controlPoint != m_selectedControlPoint)
                {
                    m_selectedControlPoint = controlPoint;

                    controlPoint.Focussed = false;

                    m_foreControlPoint = null;

                    SpawnControlPointModeBeginEvent(m_selectedControlPoint, null);
                }

                this.Invalidate();
            }

            /// <summary>
            /// Clears the currently selected node.
            /// </summary>
            public void ClearSelectedNode()
            {
                if (m_selectedNode != null)
                {
                    m_selectedNode.Highlighted = false;

                    if (ShowTraversalHighlight)
                    {
                        foreach (IGroupNode node in m_highlightedNodes)
                        {
                            node.Highlighted = false;
                        }

                        m_highlightedNodes = null;
                    }
                }

                m_selectedNode = null;

                m_controlPointMgr.Clear();

                this.Invalidate();
            }

            /// <summary>
            /// Sets the currently selected node by title.
            /// </summary>
            /// <param name="title">The title of the node to select.</param>
            public void SelectNodeByTitle(string title)
            {
                IGroupNode node = m_svg.GetGnodeByTitle(title);

                if (node != null)
                {
                    SetSelectedNode(node);
                }
            }

            /// <summary>
            /// Draws an arrow when a transition is being modified/added.
            /// </summary>
            private void m_editArrow_OnUpdateTick(double deltaTimeInSeconds)
            {
                Matrix m = Transform.Clone();

                if (m_selectedControlPoint != null)
                {
                    ControlPoint other = m_controlPointMgr.ControlPoints.FirstOrDefault(
                        cp => cp != m_selectedControlPoint && m_selectedControlPoint.Owner.Container == cp.Owner.Container);

                    if (other != null && other.Target != null)
                    {
                        m.Multiply(other.Target.HeirarchyTransform);

                        if (other.Type == ControlPoint.ControlPointType.head)
                        {
                            m_editArrow.Destination = MathUtils.TransformPoint(m, other.Target.Centre);
                            m_editArrow.Source = IsMouseOverAGraphNode() ? ForeNodeCentre : m_currentMousePos;
                        }
                        else if (other.Type == ControlPoint.ControlPointType.tail)
                        {
                            m_editArrow.Source = MathUtils.TransformPoint(m, other.Target.Centre);
                            m_editArrow.Destination = IsMouseOverAGraphNode() ? ForeNodeCentre : m_currentMousePos;
                        }

                        m_editArrow.Loop = IsMouseOverAGraphNode() && m_foreNode == other.Target;
                    }

                    m_editArrow.Snapping = IsMouseOverAGraphNode();

                    m_editArrow.Visible = true;
                }
                else if (m_addingTransition && m_sourceNode != null)
                {
                    m.Multiply(m_sourceNode.HeirarchyTransform);

                    m_editArrow.Source = MathUtils.TransformPoint(m, m_sourceNode.Centre);
                    m_editArrow.Destination = IsMouseOverAGraphNode() ? ForeNodeCentre : m_currentMousePos;
                    m_editArrow.Loop = IsMouseOverAGraphNode() && m_foreNode == m_sourceNode;
                    m_editArrow.Snapping = IsMouseOverAGraphNode();
                    m_editArrow.Visible = true;
                }
                else
                {
                    m_editArrow.Visible = false;
                }
            }
        }
    }
}
