using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Graph.NET.Utils;
using System.Linq;

namespace Graph.NET
{
    namespace GraphControls
    {
        public partial class GraphContainerPanel : UserControl
        {
            #region Fields

            protected GraphicsPanel m_graphPanel;

            protected bool m_panning = false;
            protected Point m_panningPos;

            #endregion

            #region Properties

            /// <summary>
            /// Provides access to the GraphicPanel.
            /// </summary>
            [Browsable(false)]
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
            public GraphicsPanel GraphPanel
            {
                get { return m_graphPanel; }
            }

            /// <summary>
            /// Controls the (re)loading of the graph panel.
            /// </summary>
            [Browsable(false)]
            [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
            public ISvgType Svg
            {
                get { return m_graphPanel.Svg; }
                set
                {
                    this.SuspendLayout();

                    m_graphPanel.Svg = value;

                    //SetZoomPercent(100);
                    UpdateGraphPanelSize();

                    this.ResumeLayout(true);
                    this.PerformLayout();

                    m_zoomSlider.Enabled = (value != null);
                }
            }

            #endregion

            public GraphContainerPanel()
            {
                InitializeComponent();

                m_graphPanel = new GraphicsPanel()
                {
                    BorderStyle = BorderStyle.None,
                    BackColor = this.BackColor
                };

                m_graphPanel.MouseUp += new MouseEventHandler(m_graphPanel_MouseUp);
                m_graphPanel.MouseDown += new MouseEventHandler(m_graphPanel_MouseDown);
                m_graphPanel.MouseMove += new MouseEventHandler(m_graphPanel_MouseMove);
                m_graphPanel.MouseClick += new MouseEventHandler(m_graphPanel_MouseClick);

                m_graphPanel.OnScaleUpdate += new EventHandler((s,a) =>
                {
                    m_zoomSlider.Value = (int)(m_graphPanel.Scale.Width * 100);
                });

                m_centerPanel.Controls.Add(m_graphPanel);

                CreateZoomButton(m_plusLabel, 1);
                CreateZoomButton(m_minusLabel, -1);
            }

            /// <summary>
            /// Creates a zoom "button" using a label and background panel.
            /// Event handlers are added so that the background changes colour on mouseover,
            /// and clicking the label will change the zoom in the specified direction
            /// based on the LargeChange property of the zoom slider control.
            /// </summary>
            /// <param name="label">The text label which the user will click on</param>
            /// <param name="background">The background panel behind the label which will change colour</param>
            /// <param name="direction">The direction the zoom should change when clicking the button</param>
            private void CreateZoomButton(Label label, int direction)
            {
                label.MouseEnter += delegate(object o, EventArgs e)
                {
                    label.BackColor = Color.Gainsboro;
                };

                label.MouseLeave += delegate(object o, EventArgs e)
                {
                    label.BackColor = Color.Transparent;
                };

                label.Click += delegate(object o, EventArgs e)
                {
                    SetZoomPercent(m_zoomSlider.Value + m_zoomSlider.LargeChange * direction);
                };
            }

            /// <summary>
            /// Sets the zoom slider to the specified value, constraining it to the allowed min and max values of the slider.
            /// The graph panel itself will be resized and redrawn due to the ValueChanged event of the slider control.
            /// </summary>
            /// <param name="newValue">The new zoom level to set</param>
            protected void SetZoomPercent(int newValue)
            {
                if (m_graphPanel.SvgLoaded)
                {
                    m_zoomSlider.Value = Math.Max(Math.Min(m_zoomSlider.Maximum, newValue), m_zoomSlider.Minimum);
                }
            }

            /// <summary>
            /// Handles the MouseWheel event for the GraphPanel. (Allows manual zooming/scaling of the graph.)
            /// </summary>
            /// <remarks>Public because the MainWindow must add this handler for the callback to work
            /// </remarks>
            public virtual void GraphPanel_MouseWheel(object sender, MouseEventArgs e)
            {
                if (m_graphPanel.SvgLoaded)
                {
                    int percentChange = e.Delta * SystemInformation.MouseWheelScrollLines / 120;

                    SetZoomPercent(m_zoomSlider.Value + percentChange);
                }
            }

            /// <summary>
            /// Updates the graph panel based on the current zoom level, container size and scroll position.
            /// </summary>
            protected void UpdateGraphPanelSize()
            {
                if (m_graphPanel.SvgLoaded)
                {
                    float scale = m_zoomSlider.Value / 100.0f;

                    m_graphPanel.Scale = new SizeF(scale, scale);

                    SizeF size = new SizeF(
                        m_graphPanel.SvgSize.Width * scale,
                        m_graphPanel.SvgSize.Height * scale);

                    Size minSize = new Size(
                        m_centerPanel.Width - 20,
                        m_centerPanel.Height - 20);

                    if (size.Width < minSize.Width)
                    {
                        size.Width = minSize.Width;
                    }
                    if (size.Height < minSize.Height)
                    {
                        size.Height = minSize.Height;
                    }

                    m_graphPanel.Size = Size.Round(size);

                    PerformLayout();

                    m_graphPanel.Invalidate();
                }
            }

            /// <summary>
            /// Clears the selected node when the container panel area is clicked.
            /// </summary>
            /// <remarks>
            /// We need to be able to click the container panel to clear as well because
            /// both panels are white and seamless.
            /// </remarks>
            protected override void OnMouseClick(MouseEventArgs e)
            {
                m_graphPanel.ClearSelectedNode();
            }

            /// <summary>
            /// Handles mouse move events over the GraphPanel.
            /// </summary>
            private void m_graphPanel_MouseMove(object sender, MouseEventArgs e)
            {
                if (m_panning)
                {
                    Point scrollPos = m_centerPanel.AutoScrollPosition;

                    scrollPos.X += (e.X - m_panningPos.X);
                    scrollPos.Y += (e.Y - m_panningPos.Y);

                    m_centerPanel.AutoScrollPosition = new Point(-scrollPos.X, -scrollPos.Y);
                }
            }

            /// <summary>
            /// Handles the mouse up event on the GraphPanel.
            /// </summary>
            private void m_graphPanel_MouseUp(object sender, MouseEventArgs e)
            {
                if (m_panning)
                {
                    m_panning = false;
                    Cursor = Cursors.Default;
                }
            }

            /// <summary>
            /// Handles the mouse down event on the GraphPanel.
            /// </summary>
            private void m_graphPanel_MouseDown(object sender, MouseEventArgs e)
            {
                if (e.Button == MouseButtons.Middle)
                {
                    m_panning = true;
                    m_panningPos = e.Location;
                    Cursor = Cursors.SizeAll;
                }
            }

            /// <summary>
            /// Handles the mouse click event on the GraphPanel.
            /// </summary>
            private void m_graphPanel_MouseClick(object sender, MouseEventArgs e)
            {
                // Ensures that mouse-wheel events will work when the panel is clicked.
                // Otherwise user will have to click the actual tab to regain focus.
                if (!Parent.Focused)
                {
                    Parent.Focus();
                }
            }

            /// <summary>
            /// Resizes the graph panel to fit nicely in it's parent when it's
            /// parent is resized.
            /// </summary>
            protected override void OnResize(EventArgs e)
            {
                if (m_graphPanel != null)
                {
                    UpdateGraphPanelSize();
                }
            }

            /// <summary>
            /// Resizes the graph panel when the zoom level changes and updates the
            /// slider tooltip text.
            /// </summary>
            private void m_zoomSlider_ValueChanged(object sender, EventArgs e)
            {
                UpdateGraphPanelSize();

                m_toolTip.SetToolTip(m_zoomSlider, String.Format("{0}%", m_zoomSlider.Value));
            }
        }

        public class LoadGraphEventArgs : EventArgs
        {
            private bool m_success = false;
            private string m_errorMsg = String.Empty;

            public bool Success
            {
                get { return m_success; }
            }

            public string ErrorMessage
            {
                get { return m_errorMsg; }
            }

            public LoadGraphEventArgs(bool success, string errorMsg)
            {
                m_success = success;
                m_errorMsg = errorMsg;
            }
        }
    }
}
