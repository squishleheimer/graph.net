﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Graph.NET.Utils;

namespace Graph.NET.GraphControls
{
    public partial class GraphicsPanel
    {
        /// <summary>
        /// True when a transition is nascent and an
        /// EditArrow should be drawn.
        /// </summary>
        private bool m_addingTransition = false;
        
        /// <summary>
        /// For visualizing primary graph feedback like
        /// cursor hovering over/selecting graph elements.
        /// </summary>
        private IGroupNode m_foreNode = null;
        private IGroupNode m_selectedNode = null;

        /// <summary>
        /// For adding new transitions.
        /// </summary>
        private IGroupNode m_sourceNode = null;
        private IGroupNode m_destinationNode = null;

        /// <summary>
        /// For editing existing transitions.
        /// </summary>
        private ControlPoint m_foreControlPoint = null;
        private ControlPoint m_selectedControlPoint = null;
        
        /// <summary>
        /// Tracks current cursor position.
        /// </summary>
        private PointF m_currentMousePos = PointF.Empty;

        private PointF m_mousePosDown = PointF.Empty;

        private bool m_mouseDown = false;
        
        /// <summary>
        /// Allows customisation of what mouse 
        /// button is used to edit the graph.
        /// </summary>
        private MouseButtons m_primaryMouseButton = MouseButtons.Left;
        
        public MouseButtons PrimaryMouseButton
        {
            get { return m_primaryMouseButton; }
            set { m_primaryMouseButton = value; }
        }

        public bool AddingTransition
        {
            get { return m_addingTransition; }
            set { m_addingTransition = value; }
        }

        public PointF MousePosDown
        {
            get { return m_mousePosDown; }
        }

        public PointF MousePosCurrent
        {
            get { return m_currentMousePos; }
        }

        public bool IsMouseDown
        {
            get { return m_mouseDown; }
        }

        public IGroupNode SelectedNode
        {
            get { return m_selectedNode; }
        }

        #region Event Handlers

        public event GroupNodeEventHandler GroupNodeDeselectionEvent;
        public event GroupNodeEventHandler GroupNodeSelectionEvent;
        public event GroupNodeEventHandler GroupNodeMouseEnterEvent;
        public event GroupNodeEventHandler GroupNodeMouseExitEvent;
        public event GroupNodeEventHandler GroupNodeRemovedEvent;

        public event ControlPointEditHandler ControlPointMoveEnd;
        public event ControlPointEditHandler ControlPointMoveBegin;
        public event AddTransitionHandler TransitionAdded;

        #endregion

        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            m_currentMousePos = e.Location;

            if (SvgLoaded)
            {
                if (e.Button == MouseButtons.None)
                {
                    UpdateTooltip(m_currentMousePos);
                }

                if (m_selectedControlPoint == null)
                {
                    UpdateForeControlPoint(m_currentMousePos);
                }
                else if (!m_selectedControlPoint.Active)
                {
                    m_selectedControlPoint.Active = true;
                }
                
                if (e.Button == m_primaryMouseButton)
                {
                    if (m_selectedControlPoint != null)
                    {
                        m_selectedControlPoint.Position = m_currentMousePos;
                        m_selectedControlPoint.Visible = !IsMouseOverAGraphNode();
                    }
                    else if (m_sourceNode != null && m_sourceNode != m_foreNode && m_mouseDown)
                    {
                        m_addingTransition = true;
                    }
                }
            }
        }

        private void OnMouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == m_primaryMouseButton)
            {
                if (m_foreNode != null && m_selectedControlPoint == null)
                {
                    SelectForeNode();
                }

                if (m_selectedNode != m_foreNode)
                {
                    if (m_selectedNode != null)
                    {
                        m_selectedNode.SpawnGroupNodeDeselectionEvent(m_selectedNode);

                        SpawnGroupNodeDeselectionEvent(m_selectedNode);
                    }

                    if (m_foreNode == null)
                    {
                        ClearSelectedNode();
                    }
                }
            }
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == m_primaryMouseButton)
            {
                m_mouseDown = false;

                if (m_selectedControlPoint != null)
                {
                    // If the control point was dragged over a new target then there
                    // is a valid edit and we spawn the end-edit event and clear the
                    // control points.
                    if (m_foreNode.IsOfClass(ClassType.node) && m_selectedControlPoint.Target != m_foreNode)
                    {
                        m_controlPointMgr.Clear();

                        m_highlightBox.Reset();
                        m_editArrow.Reset();

                        SpawnControlPointModeEndEvent(m_selectedControlPoint, m_foreNode);
                    }
                    // Otherwise we just re-enable everything we hid 
                    // during the edit.
                    else
                    {
                        m_selectedControlPoint.Owner.Container.Enabled = true;

                        m_selectedControlPoint.Visible = true;

                        foreach (ControlPoint cp in m_controlPointMgr.GetRelatedControlPoints(m_selectedControlPoint))
                        {
                            cp.Visible = true;
                        }
                    }

                    m_selectedControlPoint.Active = false;
                }
                else
                {
                    if (m_addingTransition && m_sourceNode != null)
                    {
                        m_highlightBox.Reset();

                        if (TransitionAdded != null)
                        {
                            TransitionAdded(this, new AddTransitionEventArgs(m_sourceNode, m_destinationNode));
                        }
                    }
                }

                m_selectedControlPoint = null;

                m_sourceNode = null;
                m_destinationNode = null;
                m_addingTransition = false;
            }
        }

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == m_primaryMouseButton)
            {
                m_mouseDown = true;
                m_mousePosDown = e.Location;
                m_sourceNode = null;
                m_destinationNode = null;

                if (m_foreControlPoint != null)
                {
                    m_foreControlPoint.Owner.Container.Enabled = false;

                    m_foreControlPoint.Visible = false;

                    SetSelectedControlPoint(m_foreControlPoint);
                }
                else if (m_foreNode.IsOfClass(ClassType.node))
                {
                    ClearSelectedNode();

                    m_sourceNode = m_foreNode;
                }
            }
        }
    }
}
