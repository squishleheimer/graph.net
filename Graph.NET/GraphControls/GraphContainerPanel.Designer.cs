namespace Graph.NET
{
    namespace GraphControls
    {
        partial class GraphContainerPanel
        {
            /// <summary> 
            /// Required designer variable.
            /// </summary>
            private System.ComponentModel.IContainer components = null;

            /// <summary> 
            /// Clean up any resources being used.
            /// </summary>
            /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
            protected override void Dispose(bool disposing)
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }

            #region Component Designer generated code

            /// <summary> 
            /// Required method for Designer support - do not modify 
            /// the contents of this method with the code editor.
            /// </summary>
            private void InitializeComponent()
            {
            this.components = new System.ComponentModel.Container();
            this.m_sidePanel = new System.Windows.Forms.Panel();
            this.m_plusLabel = new System.Windows.Forms.Label();
            this.m_minusLabel = new System.Windows.Forms.Label();
            this.m_zoomSlider = new System.Windows.Forms.TrackBar();
            this.m_centerPanel = new System.Windows.Forms.Panel();
            this.m_toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.m_sidePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_zoomSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // m_sidePanel
            // 
            this.m_sidePanel.Controls.Add(this.m_plusLabel);
            this.m_sidePanel.Controls.Add(this.m_minusLabel);
            this.m_sidePanel.Controls.Add(this.m_zoomSlider);
            this.m_sidePanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.m_sidePanel.Location = new System.Drawing.Point(0, 0);
            this.m_sidePanel.Name = "m_sidePanel";
            this.m_sidePanel.Size = new System.Drawing.Size(35, 241);
            this.m_sidePanel.TabIndex = 0;
            // 
            // m_plusLabel
            // 
            this.m_plusLabel.BackColor = System.Drawing.Color.Transparent;
            this.m_plusLabel.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_plusLabel.ForeColor = System.Drawing.Color.Transparent;
            this.m_plusLabel.Image = global::Graph.NET.Properties.Resources.magnify_plus;
            this.m_plusLabel.Location = new System.Drawing.Point(4, 3);
            this.m_plusLabel.Name = "m_plusLabel";
            this.m_plusLabel.Size = new System.Drawing.Size(25, 25);
            this.m_plusLabel.TabIndex = 1;
            this.m_toolTip.SetToolTip(this.m_plusLabel, "Increase zoom level");
            // 
            // m_minusLabel
            // 
            this.m_minusLabel.BackColor = System.Drawing.Color.Transparent;
            this.m_minusLabel.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_minusLabel.ForeColor = System.Drawing.Color.Transparent;
            this.m_minusLabel.Image = global::Graph.NET.Properties.Resources.magnify_minus;
            this.m_minusLabel.Location = new System.Drawing.Point(4, 186);
            this.m_minusLabel.Name = "m_minusLabel";
            this.m_minusLabel.Size = new System.Drawing.Size(25, 25);
            this.m_minusLabel.TabIndex = 1;
            this.m_toolTip.SetToolTip(this.m_minusLabel, "Decrease zoom level");
            // 
            // m_zoomSlider
            // 
            this.m_zoomSlider.Enabled = false;
            this.m_zoomSlider.LargeChange = 10;
            this.m_zoomSlider.Location = new System.Drawing.Point(3, 23);
            this.m_zoomSlider.Maximum = 200;
            this.m_zoomSlider.Minimum = 50;
            this.m_zoomSlider.Name = "m_zoomSlider";
            this.m_zoomSlider.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.m_zoomSlider.Size = new System.Drawing.Size(45, 170);
            this.m_zoomSlider.TabIndex = 0;
            this.m_zoomSlider.TickFrequency = 5;
            this.m_toolTip.SetToolTip(this.m_zoomSlider, "Zoom");
            this.m_zoomSlider.Value = 100;
            this.m_zoomSlider.ValueChanged += new System.EventHandler(this.m_zoomSlider_ValueChanged);
            // 
            // m_centerPanel
            // 
            this.m_centerPanel.AutoScroll = true;
            this.m_centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_centerPanel.Location = new System.Drawing.Point(35, 0);
            this.m_centerPanel.Name = "m_centerPanel";
            this.m_centerPanel.Size = new System.Drawing.Size(115, 241);
            this.m_centerPanel.TabIndex = 1;
            // 
            // GraphContainerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.m_centerPanel);
            this.Controls.Add(this.m_sidePanel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.Name = "GraphContainerPanel";
            this.Size = new System.Drawing.Size(150, 241);
            this.m_sidePanel.ResumeLayout(false);
            this.m_sidePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_zoomSlider)).EndInit();
            this.ResumeLayout(false);

            }

            #endregion

            private System.Windows.Forms.Panel m_sidePanel;
            private System.Windows.Forms.ToolTip m_toolTip;
            private System.Windows.Forms.Label m_minusLabel;
            private System.Windows.Forms.Label m_plusLabel;
            protected System.Windows.Forms.TrackBar m_zoomSlider;
            protected System.Windows.Forms.Panel m_centerPanel;
        }
    }
}
