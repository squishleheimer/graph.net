using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Graph.NET.Utils;

namespace Graph.NET
{
    public partial class svgType : AbstractGroupNode, ISvgType
    {
        #region Fields
        private string m_path;
        private GraphicsUnit m_units;
        private RectangleF m_viewBox;        
        #endregion

        #region Properties

        [XmlIgnore]
        public string FilePath
        {
            get { return m_path; }
            set { m_path = value; }
        }

        [XmlIgnore]
        public GraphicsUnit Units
        {
            get { return m_units; }
            set { m_units = value; }
        }

        [XmlIgnore]
        public System.Drawing.SizeF Dimensions
        {
            get { return m_viewBox.Size; }
            set { m_viewBox.Size = value; }
        }

        [XmlIgnore]
        public System.Drawing.RectangleF ViewBox
        {
            get { return m_viewBox; }
            set { m_viewBox = value; }
        }       

        #endregion

        #region Static Methods
        public static ISvgType Load(string filename)
        {
            try
            {
                using (FileStream stream = new FileStream(filename, FileMode.Open))
                {
                    return new XmlSerializer(typeof(svgType)).Deserialize(stream) as svgType;
                }
            }
            catch (System.Exception ex)
            {
                DumpException(ex);

                throw new ApplicationException("Error Deserialising.", ex);
            }
        }

        public static ISvgType Load(StreamReader stream)
        {
            try
            {
                return new XmlSerializer(typeof(svgType)).Deserialize(stream) as svgType;
            }
            catch (System.Exception ex)
            {
                DumpException(ex);

                throw new ApplicationException("Error Deserialising.", ex);
            }
        }

        public static void Save(string filename, ISvgType svgObj)
        {
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }

            try
            {
                using (Stream stream = new FileStream(filename, FileMode.OpenOrCreate))
                {
                    new XmlSerializer(typeof(svgType)).Serialize(stream, svgObj);
                }
            }
            catch (System.Exception ex)
            {
                DumpException(ex);

                throw new ApplicationException("Error Serialising.", ex);
            }
        }

        public static ISvgType Save(ISvgType svgObj)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                using (StringWriter writer = new StringWriter(sb))
                {
                    new XmlSerializer(typeof(svgType)).Serialize(writer, svgObj);

                    byte[] bytes = new System.Text.UTF8Encoding().GetBytes(sb.ToString());

                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        return Load(new StreamReader(ms));
                    }
                }
            }
            catch (System.Exception ex)
            {
                DumpException(ex);

                throw new ApplicationException("Error Serialising.", ex);
            }
        }

        /// <summary>
        /// Creates the ISvgType deserialised object from the svgCommand.
        /// </summary>
        /// <param name="svgCommand">The svg data.</param>
        /// <returns>The deserialised ISvgType object.</returns>
        public static ISvgType CreateSvgType(string svgCommand)
        {
            ISvgType svg = null;

            // convert string to stream
            using (MemoryStream stream = new MemoryStream())
            {
                using (StreamWriter writer = new StreamWriter(stream))
                {
                    writer.Write(svgCommand);
                    writer.Flush();

                    stream.Position = 0;

                    using (StreamReader reader = new StreamReader(stream))
                    {
                        svg = svgType.Load(reader);
                    }
                }
            }

            return svg;
        }

        #endregion

        #region DEBUG Methods
        public static void DumpException(Exception ex)
        {
            WriteExceptionInfo(ex);

            if (null != ex.InnerException)
            {
                WriteExceptionInfo(ex.InnerException);
            }
        }

        public static void WriteExceptionInfo(Exception ex)
        {
            Console.WriteLine("--------- Exception Data ---------");
            Console.WriteLine("Message: {0}", ex.Message);
            Console.WriteLine("Exception Type: {0}", ex.GetType().FullName);
            Console.WriteLine("Source: {0}", ex.Source);
            Console.WriteLine("StrackTrace: {0}", ex.StackTrace);
            Console.WriteLine("TargetSite: {0}", ex.TargetSite);
        }
        #endregion

        public ISvgType Init(string path)
        {
            FilePath = path;
            Units = GraphicsUnit.Pixel;
            Dimensions = SizeF.Empty;
            ViewBox = Rectangle.Empty;

            Adapt();

            return this;
        }

        private void Adapt()
        {
            RetrieveViewData();

            foreach (object obj in this.itemsField)
            {
                if (obj is IGroupNode)
                {
                    IGroupNode node = obj as IGroupNode;

                    node.Adapt(this);
                }

                if (obj is IShapeType)
                {
                    IShapeType shape = obj as IShapeType;

                    shape.Adapt(this, this);
                }
            }
        }

        private void RetrieveViewData()
        {
            if (this.width != null && this.height != null)
            {
                Dimensions = ConversionUtils.ParseDimensions(this.width, this.height, m_units);
            }

            if (this.viewBox != null)
            {
                string[] split = Regex.Split(this.viewBox, " ");

                if (split.Length == 4)
                {
                    PointF location = new PointF(float.Parse(split[0], CultureInfo.InvariantCulture), float.Parse(split[1], CultureInfo.InvariantCulture));
                    SizeF size = new SizeF(float.Parse(split[2], CultureInfo.InvariantCulture), float.Parse(split[3], CultureInfo.InvariantCulture));

                    ViewBox = new RectangleF(location, size);
                }
            }
        }

        public IGroupNode FocusNode(PointF p)
        {
            IEnumerable<IGroupNode> nodes = IntersectsPoint(p);

            if (nodes.Any())
            {
                // Order by proximity to cursor position so that the FirstOrDefault
                // is guaranteed to be the closest to the cursor.
                nodes = nodes.OrderBy(g => g.Proximity);

                if (nodes.Any(g => g.IsOfClass(ClassType.node | ClassType.edge)))
                {
                    return nodes.FirstOrDefault(g => g.IsOfClass(ClassType.node | ClassType.edge));
                }
                else if (nodes.Any(g => g.IsOfClass(ClassType.cluster)))
                {
                    IEnumerable<IGroupNode> clusters = nodes.Where(g => g.IsOfClass(ClassType.cluster));

                    if (clusters.Count() > 1)
                    {
                        IGroupNode smallest = clusters.First();

                        foreach (IGroupNode cluster in clusters)
                        {
                            if (smallest.Bounds.Contains(cluster.Bounds))
                            {
                                smallest = cluster;
                            }
                        }

                        return smallest;
                    }

                    return nodes.FirstOrDefault(g => g.IsOfClass(ClassType.cluster));
                }
                else if (nodes.Any(g => g.IsOfClass(ClassType.graph, false)))
                {
                    return nodes.FirstOrDefault(g => g.IsOfClass(ClassType.graph));
                }
            }

            return null;
        }

        public override ClassType Class
        {
            get
            {
                ClassType c = ClassType.invalid;

                if (this.classField != null)
                {
                    Enum.TryParse(this.classField[0], out c);
                }

                return c;
            }
        }

        public override object[] GetItems()
        {
            return this.itemsField;
        }

        public override string Id
        {
            get
            {
                if (this.id != null)
                {
                    return this.id;
                }

                return String.Empty;
            }
        }

        public override bool RemoveItem(IGroupNode node)
        {
            bool removed = false;

            ArrayList list = new ArrayList(GetItems());

            int length = list.Count;

            list.Remove(node);

            removed = length == list.Count + 1;

            this.itemsField = list.ToArray();

            return removed;
        }

        public override string Transform()
        {
            return null;
        }

        [XmlIgnore]
        public override string ToolTipText
        {
            get { return String.Empty; }
        }

        public override string ToString()
        {
            return new GraphBuilder(this).ToString();
        }
    }
}
