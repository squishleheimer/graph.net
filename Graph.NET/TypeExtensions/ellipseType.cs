﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Specialized;
using Graph.NET.Utils;
using System.Xml.Serialization;
using System.Linq;
using System.Globalization;

namespace Graph.NET
{
    partial class ellipseType : AbstractShapeType
    {
        #region Properties

        [XmlIgnore]
        public PointF Position
        {
            get { return m_rectangle.Location; }
            set { m_rectangle.Location = value; }
        }

        [XmlIgnore]
        public SizeF Size
        {
            get { return m_rectangle.Size; }
            set { m_rectangle.Size = value; }
        }

        [XmlIgnore]
        public PointF Centre
        {
            get
            {
                return new PointF(Position.X + (Size.Width / 2F), Position.Y + (Size.Height / 2F));
            }
        }

        #endregion

        #region IShapeType Members

        public override void Draw(Graphics g)
        {
            GraphicsContainer c = g.BeginContainer();
            {
                g.SmoothingMode = SmoothingMode.HighQuality;

                g.MultiplyTransform(Transform);

                if (Filled)
                {
                    g.FillEllipse(new SolidBrush(FillColor), m_rectangle);
                }

                using (Pen pen = new Pen(LineColor, Thickness))
                {
                    if (DashPattern != null)
                    {
                        pen.DashPattern = DashPattern;
                    }

                    g.DrawEllipse(pen, m_rectangle);
                }                

            } g.EndContainer(c);
        }

        public override void Adapt(ISvgType svg, IGroupNode container)
        {
            m_container = container;

            float cx = float.Parse(this.cx, CultureInfo.InvariantCulture);
            float cy = float.Parse(this.cy, CultureInfo.InvariantCulture);
            float rx = float.Parse(this.rx, CultureInfo.InvariantCulture);
            float ry = float.Parse(this.ry, CultureInfo.InvariantCulture);

            this.Position = new PointF(cx - rx, cy - ry);
            this.Size = new SizeF(rx * 2f, ry * 2f);

            Color stroke = Color.Black;
            Color fill = Color.White;
            this.Filled = true;

            if (!String.IsNullOrWhiteSpace(this.fill) && this.fill.StartsWith("url", StringComparison.InvariantCultureIgnoreCase))
            {
                string iDRef = this.fill.Replace("url(#", String.Empty).TrimEnd(')');

                // TODO: set gradient fill style.
            }
            else if (this.fill != null && this.stroke != null)
            {
                stroke = ConversionUtils.GetColor(this.stroke, stroke);
                fill = ConversionUtils.GetColor(this.fill, fill);

                if (this.fill == "none")
                {
                    this.Filled = false;
                }
            }
            else if (this.style != null)
            {
                StringDictionary styleMap = ConversionUtils.ParseStyle(this.style);

                if (styleMap != null)
                {
                    if (styleMap["fill"] == "none")
                    {
                        this.Filled = false;
                    }
                    else
                    {
                        fill = ConversionUtils.GetColor(styleMap["fill"], fill);
                        this.Filled = true;
                    }

                    stroke = ConversionUtils.GetColor(styleMap["stroke"], stroke);

                    if (styleMap.ContainsKey("stroke-width"))
                    {
                        this.Thickness = uint.Parse(styleMap["stroke-width"]);
                    }
                }
            }

            this.LineColor = stroke;
            this.FillColor = fill;
        }

        public override IEnumerable<ControlPoint> CreateControlPoints()
        {
            if (Container.IsOfClass(ClassType.edge, false) && Container.Title.Contains(@"->"))
            {
                yield return new ControlPoint(MathUtils.GetRectangleCentre(Rect), this, ControlPoint.ControlPointType.head);
            }
        }

        #endregion
    }
}
