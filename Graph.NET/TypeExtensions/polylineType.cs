﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Collections.Specialized;
using Graph.NET.Utils;
using System.Xml.Serialization;

namespace Graph.NET
{
    partial class polylineType : AbstractShapeType
    {
        #region Fields
        private List<PointF> m_points = null;
        private Color m_fillColor;
        #endregion

        #region Properties
        [XmlIgnore]
        public List<PointF> Points
        {
            get
            {
                return m_points;
            }
        }
        #endregion

        #region Contructors
        public polylineType()
            : base()
        {
            m_points = new List<PointF>();

            m_fillColor = Color.Black;
        }
        #endregion

        public void AddPoint(PointF p)
        {
            m_points.Add(p);
        }

        public void AddPoints(PointF[] points)
        {
            m_points.AddRange(points);
        }

        #region IShapeType Members

        public override void Draw(Graphics g)
        {
            if (Points.Count > 0)
            {
                PointF[] points = Points.ToArray();

                GraphicsContainer c = g.BeginContainer();
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;

                    g.MultiplyTransform(Transform);

                    if (Filled)
                    {
                        g.FillPolygon(new SolidBrush(FillColor), points);
                    }

                    using (Pen pen = new Pen(LineColor, Thickness))
                    {
                        if (DashPattern != null)
                        {
                            pen.DashPattern = DashPattern;
                        }

                        g.DrawPolygon(pen, points);
                    }

                } g.EndContainer(c);
            }
        }

        public override void Adapt(ISvgType svg, IGroupNode container)
        {
            m_container = container;

            this.AddPoints(ConversionUtils.ParsePointFs(this.points));

            Color stroke = Color.Black;
            Color fill = Color.White;

            if (this.fill != null && this.stroke != null)
            {
                stroke = ConversionUtils.GetColor(this.stroke, stroke);
                fill = ConversionUtils.GetColor(this.fill, fill);
            }
            else if (this.style != null)
            {
                StringDictionary styleMap = ConversionUtils.ParseStyle(this.style);

                if (styleMap != null)
                {
                    if (styleMap["fill"] == "none")
                    {
                        this.Filled = false;
                    }
                    else
                    {
                        fill = ConversionUtils.GetColor(styleMap["fill"], fill);
                    }

                    stroke = ConversionUtils.GetColor(styleMap["stroke"], stroke);

                    if (styleMap.ContainsKey("stroke-width"))
                    {
                        this.Thickness = uint.Parse(styleMap["stroke-width"]);
                    }
                }
            }

            this.LineColor = stroke;
            this.FillColor = fill;

            this.Filled = false;

            Rect = ConversionUtils.RectangleFFromPoints(Points.ToArray());
        }

        #endregion
    }
}
