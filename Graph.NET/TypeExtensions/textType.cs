﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using Graph.NET;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Graph.NET.Utils;
using System.Xml.Serialization;
using System.Globalization;

namespace Graph.NET
{
    public enum TextAnchor
    {
        Start,
        Middle,
        End
    }

    partial class textType : AbstractShapeType
    {
        #region Fields
        private Font m_font;
        private string m_text;
        private PointF m_position;
        private SizeF m_size;
        private TextAnchor m_anchor = TextAnchor.Start;
        #endregion

        #region Properties
        [XmlIgnore]
        public PointF Position
        {
            get { return m_position; }
            set { m_position = value; }
        }

        [XmlIgnore]
        public SizeF Size
        {
            get { return m_size; }
            set { m_size = value; }
        }

        [XmlIgnore]
        public string Glyphs
        {
            get { return m_text; }
            set { m_text = value; }
        }

        [XmlIgnore]
        public Font Font
        {
            get { return m_font; }
            set { m_font = value; }
        }

        [XmlIgnore]
        public GraphicsUnit Units
        {
            get { return m_font.Unit; }
            set { m_font = new Font(m_font.FontFamily, m_font.Size, value); }
        }

        [XmlIgnore]
        public TextAnchor Anchor
        {
            get { return m_anchor; }
            set { m_anchor = value; }
        }
        #endregion

        #region Constructors
        public textType()
            : base()
        {
            m_text = String.Empty;
            m_font = new Font(@"Arial", 9);
        }
        #endregion

        #region IShapeType Members
        public override void Draw(Graphics g)
        {
            if (!String.IsNullOrEmpty(Glyphs))
            {
                GraphicsContainer c = g.BeginContainer();
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;

                    g.MultiplyTransform(Transform);

                    using (Pen pen = new Pen(LineColor, Thickness))
                    {
                        Size = g.MeasureString(Glyphs, Font);

                        float sizeDelta = Font.Size / Font.SizeInPoints;

                        float xOffset = 0;
                        float yOffset = (Size.Height / 2.0f) * sizeDelta;

                        if (Anchor == TextAnchor.Middle)
                        {
                            xOffset = Size.Width / 2.0f;
                        }
                        if (Anchor == TextAnchor.End)
                        {
                            xOffset = -Size.Width / 2.0f;
                        }

                        PointF drawPosition = Position - new SizeF(xOffset, yOffset);

                        g.DrawString(Glyphs, Font, pen.Brush, drawPosition);

                        if (!Size.IsEmpty)
                        {
                            if (Rect.IsEmpty)
                            {
                                m_rectangle.Location = drawPosition;
                                m_rectangle.Size = Size;
                            }
                        }
                    }

                } g.EndContainer(c);
            }
        }

        public override void Adapt(ISvgType svg, IGroupNode container)
        {
            m_container = container;

            if (this.Text != null)
            {
                foreach (string piece in this.Text)
                {
                    this.Glyphs += piece;
                }

                this.Position = new PointF(float.Parse(this.x, CultureInfo.InvariantCulture), float.Parse(this.y, CultureInfo.InvariantCulture));
                
                if (this.fontfamily != null)
                {
                    string[] fontDetails = Regex.Split(this.fontfamily, ",");

                    this.Font = new Font(fontDetails[0], float.Parse(this.fontsize, CultureInfo.InvariantCulture), GraphicsUnit.Pixel);
                }
                else if (this.style != null)
                {
                    StringDictionary styleMap = ConversionUtils.ParseStyle(this.style);

                    if (styleMap != null)
                    {
                        this.Font = new Font(styleMap["font-family"], float.Parse(styleMap["font-size"], CultureInfo.InvariantCulture), GraphicsUnit.Pixel);
                        this.LineColor = ConversionUtils.GetColor(styleMap["fill"], Color.Black);
                    }
                }
                
                if (!String.IsNullOrWhiteSpace(this.fill))
                {
                    this.LineColor = ConversionUtils.GetColor(this.fill, this.LineColor);
                }

                if (this.textanchorSpecified)
                {
                    if (this.textanchor == trefTypeTextanchor.start)
                    {
                        this.Anchor = TextAnchor.Start;
                    }

                    if (this.textanchor == trefTypeTextanchor.middle)
                    {
                        this.Anchor = TextAnchor.Middle;
                    }

                    if (this.textanchor == trefTypeTextanchor.end)
                    {
                        this.Anchor = TextAnchor.End;
                    }
                }
            }
        }

        #endregion
    }
}
