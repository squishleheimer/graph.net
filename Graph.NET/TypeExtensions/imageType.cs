﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections.Specialized;
using System.IO;
using Graph.NET.Utils;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using System.Globalization;

namespace Graph.NET
{
    partial class imageType : AbstractShapeType
    {
        Image m_image = null;

        #region Properties

        [XmlIgnore]
        public PointF Position
        {
            get { return m_rectangle.Location; }
            set { m_rectangle.Location = value; }
        }

        [XmlIgnore]
        public SizeF Size
        {
            get { return m_rectangle.Size; }
            set { m_rectangle.Size = value; }
        }

        [XmlIgnore]
        public PointF Centre
        {
            get
            {
                return new PointF(
                Position.X + (Size.Width / 2F),
                Position.Y + (Size.Height / 2F));
            }
        }

        #endregion

        #region IShapeType Members

        public override void Adapt(ISvgType svg, IGroupNode container)
        {
            m_container = container;

            if (this.href != null)
            {
                string path = svg.FilePath;

                if (Path.HasExtension(svg.FilePath))
                {
                    path = Path.Combine(Path.GetDirectoryName(svg.FilePath), this.href);
                }
                else
                {
                    path = Path.Combine(path, this.href);
                }

                m_image = Image.FromFile(path);

                float cx = float.Parse(this.x, CultureInfo.InvariantCulture);
                float cy = float.Parse(this.y, CultureInfo.InvariantCulture);

                this.Position = new PointF(cx, cy);

                GraphicsUnit units = svg.Units;
                this.Size = ConversionUtils.ParseDimensions(this.width, this.height, units);
            }
        }

        public override void Draw(Graphics g)
        {
            if (m_image != null)
            {
                GraphicsContainer c = g.BeginContainer();
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.MultiplyTransform(Transform);
                    g.DrawImage(m_image, m_rectangle);

                } g.EndContainer(c);
            }
        }

        #endregion
    }
}
