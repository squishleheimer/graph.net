﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Xml.Serialization;

namespace Graph.NET
{
    partial class defsType : AbstractGroupNode
    {
        #region IGroupNode Members

        public override bool RemoveItem(IGroupNode node)
        {
            bool removed = false;

            ArrayList list = new ArrayList(GetItems());

            int length = list.Count;

            list.Remove(node);

            removed = length == list.Count + 1;

            this.itemsField = list.ToArray();

            return removed;
        }

        public override ClassType Class
        {
            get
            {
                ClassType c = ClassType.invalid;

                if (this.classField != null)
                {
                    Enum.TryParse(this.classField[0], out c);
                }

                return c;
            }
        }

        public override object[] GetItems()
        {
            return this.itemsField;
        }

        public override string Id
        {
            get
            {
                if (this.id != null)
                {
                    return this.id;
                }

                return String.Empty;
            }
        }

        public override string Transform()
        {
            return this.transform;
        }

        [XmlIgnore]
        public override string ToolTipText
        {
            get { return String.Empty; }
        }

        #endregion
    }
}
