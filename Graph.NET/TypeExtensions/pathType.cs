﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Graph.NET.Utils;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Linq;

namespace Graph.NET
{
    partial class pathType : AbstractShapeType
    {
        public class PathPoint
        {
            #region Fields
            private PointF m_point = PointF.Empty;
            private PathPointType m_pointType = PathPointType.Bezier;
            private char m_pointTypeChar = ' ';
            #endregion

            #region Properties
            [XmlIgnore]
            public PointF Point
            {
                get { return m_point; }
                set { m_point = value; }
            }

            [XmlIgnore]
            public PathPointType PointType
            {
                get { return m_pointType; }
                set { m_pointType = value; }
            }

            [XmlIgnore]
            public char PointTypeChar
            {
                get { return m_pointTypeChar; }
                set { m_pointTypeChar = value; }
            }
            #endregion

            public PathPoint(PointF point, char pointTypeChar)
            {
                m_point = point;
                m_pointTypeChar = pointTypeChar;
                m_pointType = ToPathPointType();
            }

            private PathPointType ToPathPointType()
            {
                switch (m_pointTypeChar)
                {
                    case 'L': // lineto
                    case 'V': // vertical lineto
                    case 'H': // horizontal lineto
                        return PathPointType.Line;
                    case 'M': // moveto
                        return PathPointType.Start;
                    case ' ':
                    case 'A': // elliptical Arc
                    case 'C': // curveto
                    case 'S': // smooth curveto
                    case 'Q': // quadratic Belzier curve
                    case 'T': // smooth quadratic Belzier curveto
                        return PathPointType.Bezier;
                    case 'Z': // closepath
                        return PathPointType.CloseSubpath;
                }

                return PathPointType.Bezier;
            }
        }

        public static readonly string TRIMCHARS = "LVHMACSQTZ ";

        #region Fields
        private List<PathPoint> m_points = null;
        #endregion

        #region Properties
        [XmlIgnore]
        public PathPoint Head
        {
            get { return m_points[m_points.Count - 1]; }
        }

        [XmlIgnore]
        public PathPoint Tail
        {
            get { return m_points[0]; }
        }

        [XmlIgnore]
        public bool ContainsPoints
        {
            get { return m_points.Count > 0; }
        }

        [XmlIgnore]
        public List<PathPoint> PathPoints
        {
            get { return m_points; }
        }

        [XmlIgnore]
        public List<PointF> Points
        {
            get
            {
                List<PointF> points = new List<PointF>();

                foreach (PathPoint p in m_points)
                {
                    points.Add(p.Point);
                }

                return points;
            }
        }

        [XmlIgnore]
        public List<byte> PointTypes
        {
            get
            {
                List<byte> pointTypes = new List<byte>();

                foreach (PathPoint p in m_points)
                {
                    pointTypes.Add((byte)p.PointType);
                }

                return pointTypes;
            }
        }

        #endregion

        #region Constructors
        public pathType()
            : base()
        {
            m_points = new List<PathPoint>();
        }
        #endregion

        public void AddPoint(PointF p, char pointType)
        {
            m_points.Add(new PathPoint(p, pointType));
        }

        #region IShapeType Members

        public override bool IntersectsPoint(PointF point)
        {
            return MathUtils.IsPointWithinDistanceOfOneOfPoints(point, Points.ToArray(), 10.0);
        }

        public override void Draw(Graphics g)
        {
            if (ContainsPoints)
            {
                GraphicsContainer c = g.BeginContainer();
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.MultiplyTransform(Transform);

                    using (Pen strokePen = new Pen(LineColor, Thickness))
                    {
                        if (DashPattern != null)
                        {
                            strokePen.DashPattern = DashPattern;
                        }

                        using (GraphicsPath gPath = new GraphicsPath(Points.ToArray(), PointTypes.ToArray()))
                        {
                            g.DrawPath(strokePen, gPath);

                            if (Filled)
                            {
                                using (Pen fillPen = new Pen(FillColor, Thickness))
                                {
                                    g.FillPath(fillPen.Brush, gPath);
                                }
                            }
                        }
                    }

                } g.EndContainer(c);
            }
        }

        public override void Adapt(ISvgType svg, IGroupNode container)
        {
            m_container = container;
            Color stroke = Color.Black;
            Color fill = Color.White;

            if (this.stroke != null)
            {
                stroke = ConversionUtils.GetColor(this.stroke, stroke);
                fill = ConversionUtils.GetColor(this.fill, fill);

                if (this.fill == "none")
                {
                    this.Filled = false;
                }
                else
                {
                    this.Filled = true;
                }
            }
            else if (this.style != null)
            {
                StringDictionary styleMap = ConversionUtils.ParseStyle(this.style);

                if (styleMap != null)
                {
                    if (styleMap["fill"] == "none")
                    {
                        this.Filled = false;
                    }
                    else
                    {
                        fill = ConversionUtils.GetColor(styleMap["fill"], fill);
                        this.Filled = true;
                    }

                    stroke = ConversionUtils.GetColor(styleMap["stroke"], stroke);

                    if (styleMap.ContainsKey("stroke-width"))
                    {
                        this.Thickness = uint.Parse(styleMap["stroke-width"]);
                    }

                    if (styleMap.ContainsKey("stroke-dasharray"))
                    {
                        string[] split = Regex.Split(styleMap["stroke-dasharray"], ",");

                        this.DashPattern = new Single[split.Length];

                        for (uint i = 0; i < split.Length; ++i)
                        {
                            this.DashPattern[i] = Single.Parse(split[i]);
                        }
                    }
                }
            }

            if (this.strokedasharray != null)
            {
                string[] split = Regex.Split(this.strokedasharray, ",");

                this.DashPattern = new Single[split.Length];

                for (uint i = 0; i < split.Length; ++i)
                {
                    this.DashPattern[i] = Single.Parse(split[i]);
                }
            }

            this.LineColor = stroke;
            this.FillColor = fill;

            string[] splitD = Regex.Split(this.d, @"([A-Z|a-z]*[\s]*[e\-\d\.*]+[,|\s]+[-*\d\.*]+)",
                RegexOptions.IgnoreCase);

            foreach (string pointStr in splitD)
            {
                string trimStr = pointStr.Trim();

                if (!String.IsNullOrEmpty(trimStr))
                {
                    char type = ' ';

                    PointF p = ConversionUtils.ParsePathPointString(
                        trimStr,
                        TRIMCHARS.ToCharArray(),
                        ref type);

                    this.AddPoint(p, type);
                }
            }

            Rect = ConversionUtils.RectangleFFromPoints(Points.ToArray());
        }

        public override IEnumerable<ControlPoint> CreateControlPoints()
        {
            if (Container.IsOfClass(ClassType.edge))
            {
                // If there is no arrowhead shape then create ControlPoint for Head.
                if (!Container.GetShapes().Where(s => s != this).Any())
                {
                    yield return new ControlPoint(Head.Point, this, ControlPoint.ControlPointType.head);
                }

                yield return new ControlPoint(Tail.Point, this, ControlPoint.ControlPointType.tail);
            }
        }

        #endregion
    }
}
