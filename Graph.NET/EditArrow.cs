﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using Graph.NET.Utils;
using Graph.NET.Utils.GraphicsExtensions;

namespace Graph.NET
{
    public class EditArrow : AbstractDrawable
    {
        #region Fields

        private double m_timePassed = 0;
        private float m_swell = 0; // Arrow head size swell.

        private PointF m_source = PointF.Empty;
        private PointF m_destination = PointF.Empty;

        private bool m_loop = false;
        private bool m_snapping = false;

        const float INITIAL_START_ANGLE = (float)(-1 * Math.PI / 1);
        const float INITIAL_SWEEP_ANGLE = (float)(4.5 * Math.PI / 3);
        float m_startAngle = INITIAL_START_ANGLE;
        float m_sweepAngle = INITIAL_SWEEP_ANGLE;
        
        #endregion

        #region Properties

        public PointF Source
        {
            get { return m_source; }
            set { m_source = value; }
        }

        public PointF Destination
        {
            get { return m_destination; }
            set { m_destination = value; }
        }

        public bool Loop
        {
            get { return m_loop; }
            set { m_loop = value; }
        }

        public bool Snapping
        {
            get { return m_snapping; }
            set { m_snapping = value; }
        }

        #endregion

        #region Public Methods

        public override void Animate(double deltaTimeInSeconds)
        {
            base.Animate(deltaTimeInSeconds);

            m_timePassed += deltaTimeInSeconds;

            const int minAlpha = 150;

            if (Snapping)
            {
                const double swellMax = 2;
                const double animSpeed = 2;

                double x = m_timePassed * animSpeed * (2.0 * Math.PI);

                m_swell = (float)MathUtils.GeneratePulseFromZeroToRange(0, swellMax, x);
                int alpha = (int)MathUtils.GeneratePulseFromZeroToRange(minAlpha, 255, x);

                FillColor = Color.FromArgb(alpha, FillColor);
                LineColor = Color.FromArgb(alpha, LineColor);
            }   
            else
            {
                m_swell = 0;

                FillColor = Color.FromArgb(minAlpha, FillColor);
                LineColor = Color.FromArgb(minAlpha, LineColor);
            }
        }

        public void Reset()
        {
            m_source = PointF.Empty;
            m_destination = PointF.Empty;
            m_loop = false;
            m_snapping = false;
            m_timePassed = 0;
        }

        public void Draw(Graphics g)
        {
            if (Visible)
            {
                using (Brush headFill = new SolidBrush(FillColor))
                using (Pen shaftStroke = new Pen(LineColor, Thickness))
                using (Pen headStroke = new Pen(LineColor, Math.Max(1, Thickness/2)))
                {
                    GraphicsContainer c = g.BeginContainer();
                    {
                        g.SmoothingMode = SmoothingMode.HighQuality;
                        
                        float headWidth  = 3f * Thickness;
                        float headLength = 3.5f * Thickness;

                        headLength += m_swell;
                        headWidth += m_swell;

                        if (Loop)
                        {
                            g.DrawLoopArrow(
                            shaftStroke,
                            headStroke,
                            headFill,
                            Destination,
                            headWidth,
                            headLength,
                            Rect.Width,
                            Rect.Height,
                            m_startAngle,
                            m_sweepAngle);
                        }
                        else 
                        {
                            g.DrawStraightArrow(
                            shaftStroke,
                            headStroke,
                            headFill,
                            Source,
                            Destination,
                            headWidth,
                            headLength);
                        }

                    } g.EndContainer(c);
                }
            }
        }

        #endregion
    }
}
