﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using Graph.NET.Utils;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Linq;
using System.Globalization;

namespace Graph.NET
{
    public abstract class AbstractGroupNode : IGroupNode
    {
        private static readonly SizeF UNIT_SQUARE = new Size(1, 1);

        public event GroupNodeEventHandler GroupNodeSelectionEvent;
        public event GroupNodeEventHandler GroupNodeDeselectionEvent;

        #region Fields

        private IGroupNode m_parent = null;
        private bool m_highlighted = false;
        private bool m_enabled = true;

        private PointF m_translation = PointF.Empty;
        private SizeF m_scale = UNIT_SQUARE;
        private float m_rotation = 0f;

        private float m_proximity = float.MaxValue;

        private List<IGroupNode> m_incomingNodes = new List<IGroupNode>();
        private List<IGroupNode> m_outgoingNodes = new List<IGroupNode>();

        #endregion

        #region IGroupNode Members

        [XmlIgnore]
        public bool Highlighted
        {
            get { return m_highlighted; }
            set { m_highlighted = value; }
        }

        [XmlIgnore]
        public bool Enabled
        {
            get { return m_enabled; }
            set { m_enabled = value; }
        }

        [XmlIgnore]
        public float Rotation
        {
            get { return m_rotation; }
            set { m_rotation = value; }
        }

        [XmlIgnore]
        public PointF Translation
        {
            get { return m_translation; }
            set { m_translation = value; }
        }

        [XmlIgnore]
        public SizeF Scale
        {
            get { return m_scale; }
            set
            {
                if (value.Width > 0.0001f && value.Height > 0.0001f)
                {
                    m_scale = value;
                }
            }
        }

        [XmlIgnore]
        public Matrix LocalTransform
        {
            get { return GetTransformDataAsMatrix(); }
        }

        [XmlIgnore]
        public IGroupNode Parent
        {
            get { return m_parent; }
            set { m_parent = value; }
        }

        [XmlIgnore]
        public float Proximity
        {
            get { return m_proximity; }
        }

        public void Adapt(ISvgType svg)
        {
            RetrieveTransformationData();

            foreach (IGroupNode node in GetGnodes())
            {
                node.Parent = this;
                node.Adapt(svg);
            }

            foreach (IShapeType shape in GetShapes())
            {
                shape.Adapt(svg, this);
            }

            if (this.IsOfClass(ClassType.node, false))
            {
                m_incomingNodes.AddRange(GraphUtils.GetIncomingNodes(svg, this));
                m_outgoingNodes.AddRange(GraphUtils.GetOutgoingNodes(svg, this));
            }
            else if (this.IsOfClass(ClassType.edge, false))
            {
                m_incomingNodes.AddRange(GraphUtils.GetSourceNode(svg, this));
                m_outgoingNodes.AddRange(GraphUtils.GetDestinationNode(svg, this));
            }
        }

        public Matrix GetTransformDataAsMatrix()
        {
            Matrix m = new Matrix();

            m.Rotate(m_rotation);
            m.Scale(m_scale.Width, m_scale.Height);
            m.Translate(m_translation.X, m_translation.Y);

            return m;
        }

        public virtual void Draw(Graphics g)
        {
            if (Enabled)
            {
                GraphicsContainer c = g.BeginContainer();
                {
                    g.MultiplyTransform(LocalTransform);

                    foreach (IShapeType shape in GetShapes().Where(shape => shape.Enabled))
                    {
                        AbstractDrawable drawable = null;
                        uint thickness = 1;
                        Color fill = Color.Red;
                        Color line = Color.Black;

                        if (Highlighted && this.IsOfClass(ClassType.node | ClassType.edge | ClassType.cluster))
                        {
                            if (shape is AbstractDrawable)
                            {
                                drawable = shape as AbstractDrawable;
                                thickness = drawable.Thickness;
                                fill = drawable.FillColor;
                                line = drawable.LineColor;

                                drawable.Thickness += 2;
                                drawable.FillColor = Color.DarkGray;
                                drawable.LineColor = Color.DarkGray;

                                shape.Draw(g);

                                drawable.Thickness = thickness;
                                drawable.FillColor = fill;
                                drawable.LineColor = line;
                            }
                        }

                        shape.Draw(g);
                    }

                    foreach (IGroupNode node in GetGnodes().Where(node => node.Enabled))
                    {
                        node.Draw(g);
                    }
                }
                g.EndContainer(c);
            }
        }

        public void SpawnGroupNodeSelectionEvent(IGroupNode node)
        {
            if (node != null && GroupNodeSelectionEvent != null)
            {
                GroupNodeSelectionEvent(this, new GroupNodeEventArgs(node));
            }
        }

        public void SpawnGroupNodeDeselectionEvent(IGroupNode node)
        {
            if (node != null && GroupNodeDeselectionEvent != null)
            {
                GroupNodeDeselectionEvent(this, new GroupNodeEventArgs(node));
            }
        }

        protected void RetrieveTransformationData()
        {
            if (this.Transform() != null)
            {
                string[] items = Regex.Split(this.Transform(), @"\(|\)");

                for (uint i = 0; i < items.Length; ++i)
                {
                    string item = items[i].ToLower();

                    if (i + 1 < items.Length)
                    {
                        if (item.Contains("scale"))
                        {
                            string[] xy = Regex.Split(items[i + 1], " ");

                            m_scale = new SizeF(float.Parse(xy[0], CultureInfo.InvariantCulture), float.Parse(xy[1], CultureInfo.InvariantCulture));
                        }
                        if (item.Contains("rotate"))
                        {
                            m_rotation = float.Parse(items[i + 1], CultureInfo.InvariantCulture);
                        }
                        if (item.Contains("translate"))
                        {
                            string[] xy = Regex.Split(items[i + 1], " ");

                            m_translation = new PointF(float.Parse(xy[0], CultureInfo.InvariantCulture), float.Parse(xy[1], CultureInfo.InvariantCulture));
                        }
                    }
                }
            }
        }

        public RectangleF Bounds
        {
            get
            {
                RectangleF rect = RectangleF.Empty;

                List<IShapeType> shapes = new List<IShapeType>(GetShapes());

                if (shapes.Count > 0)
                {
                    rect = shapes[0].Bounds;

                    for (int i = 1; i < shapes.Count; ++i)
                    {
                        RectangleF shapeRect = shapes[i].Bounds;

                        if (!rect.IsEmpty && !shapeRect.IsEmpty)
                        {
                            rect = RectangleF.Union(rect, shapeRect);
                        }
                    }
                }

                foreach (IGroupNode node in GetGnodes())
                {
                    RectangleF nodeRect = node.Bounds;

                    if (!nodeRect.IsEmpty)
                    {
                        if (!rect.IsEmpty)
                        {
                            rect = RectangleF.Union(rect, nodeRect);
                        }
                        else
                        {
                            rect = nodeRect;
                        }
                    }
                }

                return rect;
            }
        }

        public PointF[] GetBoundCorners()
        {
            PointF[] points = MathUtils.GetRectangleCorners(Bounds);

            GetTransformDataAsMatrix().TransformPoints(points);

            return points;
        }

        public PointF Centre
        {
            get
            {
                RectangleF b = Bounds;

                return new PointF(
                    b.X + (b.Width / 2.0f),
                    b.Y + (b.Height / 2.0f));
            }
        }

        public PointF GetTransformedCentre(Matrix m)
        {
            m.Multiply(this.HeirarchyTransform);

            return MathUtils.TransformPoint(m, this.Centre);
        }

        public IEnumerable<IShapeType> GetShapes()
        {
            foreach (IShapeType shape in GetItems().Where(o => o is IShapeType))
            {
                yield return shape;
            }
        }

        public IEnumerable<IShapeType> RecursiveGetShapes()
        {
            foreach (IShapeType shape in GetItems().Where(o => o is IShapeType))
            {
                yield return shape;
            }

            foreach (IGroupNode node in GetGnodes())
            {
                foreach (IShapeType shape in node.RecursiveGetShapes())
                {
                    yield return shape;
                }
            }
        }

        public IEnumerable<IGroupNode> RecursiveGetGnodes()
        {
            foreach (IGroupNode node in GetGnodes())
            {
                foreach (IGroupNode inner in node.RecursiveGetGnodes())
                {
                    yield return inner;
                }

                yield return node;
            }
        }

        public IEnumerable<IGroupNode> RecursiveGetGnodesOfClass(ClassType nodeClass)
        {
            foreach (IGroupNode node in GetGnodes())
            {
                foreach (IGroupNode inner in node.RecursiveGetGnodesOfClass(nodeClass))
                {
                    yield return inner;
                }

                if (nodeClass == ClassType.invalid || node.IsOfClass(nodeClass, false))
                {
                    yield return node;
                }
            }
        }

        public IEnumerable<IGroupNode> GetGnodes()
        {
            foreach (IGroupNode node in GetItems().Where(o => o is IGroupNode))
            {
                yield return node;
            }
        }

        public IEnumerable<IGroupNode> GetGnodesOfClass(ClassType nodeClass)
        {
            foreach (IGroupNode node in GetItems())
            {
                if (node is IGroupNode &&
                (nodeClass == ClassType.invalid || (node as IGroupNode).IsOfClass(nodeClass, false)))
                {
                    yield return node;
                }
            }
        }

        public bool IsOfClass(ClassType nodeClass, bool checkAncestors)
        {
            if (this.Class == nodeClass)
            {
                return true;
            }

            if (checkAncestors)
            {
                IGroupNode parent = this.Parent;

                while (parent != null)
                {
                    if (parent.Class == nodeClass)
                    {
                        return true;
                    }

                    parent = parent.Parent;
                }
            }

            return false;
        }

        public IEnumerable<IGroupNode> IncomingNodes()
        {
            return m_incomingNodes;
        }

        public IEnumerable<IGroupNode> OutgoingNodes()
        {
            return m_outgoingNodes;
        }

        [XmlIgnore]
        public Matrix HeirarchyTransform
        {
            get
            {
                Matrix m = new Matrix();
                IGroupNode parent = this.Parent;

                while (parent != null)
                {
                    m.Multiply(parent.LocalTransform, MatrixOrder.Prepend);

                    parent = parent.Parent;
                }

                return m;
            }
        }

        public virtual bool HasShapes
        {
            get { return GetShapes().Any(); }
        }

        public virtual bool HasGnodes
        {
            get { return GetGnodes().Any(); }
        }

        public virtual IEnumerable<IGroupNode> IntersectsPoint(PointF p)
        {
            if (Enabled)
            {
                Matrix m = LocalTransform;
                m.Invert();

                MathUtils.TransformPoint(m, ref p);

                m_proximity = (Vector.FromPointF(p) - Vector.FromPointF(this.Centre)).LengthSq;

                if (HasGnodes)
                {
                    foreach (IGroupNode node in GetGnodes().Where(node => node.Enabled))
                    {
                        IEnumerable<IGroupNode> innerNodes = node.IntersectsPoint(p);

                        if (innerNodes.Any())
                        {
                            foreach (IGroupNode innerNode in innerNodes)
                            {
                                yield return innerNode;
                            }
                        }
                        else
                        {
                            foreach (IShapeType shape in node.GetShapes().Where(shape => shape.Enabled))
                            {
                                if (shape.IntersectsPoint(p))
                                {
                                    yield return node;
                                }
                            }
                        }
                    }
                }

                foreach (IShapeType shape in GetShapes().Where(shape => shape.Enabled))
                {
                    if (shape.IntersectsPoint(p))
                    {
                        yield return this;
                    }
                }
            }
        }

        public IGroupNode GetGnodeById(string id)
        {
            foreach (IGroupNode node in GetGnodes())
            {
                IGroupNode innerNode = node.GetGnodeById(id);

                if (innerNode != null)
                {
                    return innerNode;
                }

                if (node.Id == id)
                {
                    return node;
                }
            }

            if (Id == id)
            {
                return this;
            }

            return null;
        }

        public IGroupNode GetGnodeByTitle(string title)
        {
            foreach (IGroupNode node in GetGnodes())
            {
                IGroupNode innerNode = node.GetGnodeByTitle(title);

                if (innerNode != null)
                {
                    return innerNode;
                }

                if (node.Title == title)
                {
                    return node;
                }
            }

            if (Title == title)
            {
                return this;
            }

            return null;
        }

        [XmlIgnore]
        public string Title
        {
            get
            {
                // First attempt to get any title elements that may exists at the top level.
                foreach (titleType title in GetItems().Where(item => item.GetType() == typeof(titleType)))
                {
                    if (title.Text != null)
                    {
                        return String.Join(" ", title.Text);
                    }
                }

                // If no title elements exists at the top level then get the first available from the group nodes.
                IGroupNode node = GetGnodes().FirstOrDefault(n => n.GetItems().Any(item => item.GetType() == typeof(titleType)));

                if (node != null)
                {
                    return node.Title;
                }

                return String.Empty;
            }
        }

        [XmlIgnore]
        public string Label
        {
            get
            {
                // First attempt to get any title elements that may exists at the top level.
                foreach (textType text in GetItems().Where(item => item.GetType() == typeof(textType)))
                {
                    if (text.Text != null)
                    {
                        return String.Join(" ", text.Text);
                    }
                }

                // If no title elements exists at the top level then get the first available from the group nodes.
                IGroupNode node = GetGnodes().FirstOrDefault(n => n.GetItems().Any(item => item.GetType() == typeof(textType)));

                if (node != null)
                {
                    return node.Label;
                }

                return String.Empty;
            }
        }

        [XmlIgnore]
        public string Description
        {
            get
            {
                foreach (descType desc in GetItems().Where(item => item.GetType() == typeof(descType)))
                {
                    return String.Join(" ", desc.Text);
                }

                return String.Empty;
            }
        }

        [XmlIgnore]
        public abstract ClassType Class { get; }

        public abstract object[] GetItems();

        public abstract bool RemoveItem(IGroupNode node);

        [XmlIgnore]
        public abstract string Id { get; }

        public abstract string Transform();

        [XmlIgnore]
        public abstract string ToolTipText { get; }

        #endregion
    }
}
