﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;

namespace Graph.NET
{
    public interface ISvgType
    {
        GraphicsUnit Units { get; set; }

        SizeF Dimensions { get; set; }

        RectangleF ViewBox { get; set; }

        string FilePath { get; set; }

        string Title { get; }

        IGroupNode FocusNode(PointF p);

        IGroupNode GetGnodeById(string id);

        IGroupNode GetGnodeByTitle(string title);

        IEnumerable<IGroupNode> GetGnodes();
        IEnumerable<IGroupNode> GetGnodesOfClass(ClassType nodeClass);
        IEnumerable<IGroupNode> RecursiveGetGnodes();
        IEnumerable<IGroupNode> RecursiveGetGnodesOfClass(ClassType nodeClass);

        void Draw(Graphics g);

        ISvgType Init(string path);
    }
}
