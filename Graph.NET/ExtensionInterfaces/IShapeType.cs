﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Graph.NET
{
    public interface IShapeType
    {
        IGroupNode Container { get; }
        bool Enabled { set; get; }
        RectangleF Bounds { get; }

        void Adapt(ISvgType svg, IGroupNode container);

        bool IntersectsPoint(PointF point);
        void Draw(Graphics g);

        IEnumerable<ControlPoint> CreateControlPoints();
    }
}
