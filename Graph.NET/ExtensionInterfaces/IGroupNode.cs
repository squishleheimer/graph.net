﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Windows.Forms;

namespace Graph.NET
{
    /// <summary>
    /// Note that these aren't used as flags in the traditional
    /// sense. An IGroupNode can only be one of these types. Flags
    /// are used as they provide a convenient mechanism (bitwise)
    /// to check if an IGroupNode is one of these multiple types.
    /// (See GraphUtils.IsOfClass extension method.)
    /// </summary>
    [Flags]
    public enum ClassType
    {
        graph   = 1,
        cluster = 2,
        node    = 4,
        edge    = 8,
        invalid = 16
    }

    public class GroupNodeEventArgs : EventArgs
    {
        private IGroupNode m_node;

        public IGroupNode Node
        {
            get { return m_node; }
        }

        public GroupNodeEventArgs(IGroupNode node)
        {
            m_node = node;
        }
    }

    public class GroupNodeSelectionArgs : GroupNodeEventArgs
    {
        MouseEventArgs m_mouseArgs;

        public MouseEventArgs MouseArgs
        {
            get { return m_mouseArgs; }
        }

        public GroupNodeSelectionArgs(IGroupNode node, MouseEventArgs e)
            : base(node)
        {
            m_mouseArgs = e;
        }
    }

    public class AddTransitionEventArgs : EventArgs
    {
        private IGroupNode m_source = null;
        private IGroupNode m_destination = null;

        public IGroupNode Source
        {
            get { return m_source; }
        }

        public IGroupNode Destination
        {
            get { return m_destination; }
        }

        public AddTransitionEventArgs(IGroupNode source, IGroupNode destination)
        {
            m_source = source;
            m_destination = destination;
        }
    }
        
    public delegate void GroupNodeEventHandler(object sender, GroupNodeEventArgs e);
    public delegate void AddTransitionHandler(object sender, AddTransitionEventArgs e);

    public interface IGroupNode
    {
        event GroupNodeEventHandler GroupNodeSelectionEvent;
        event GroupNodeEventHandler GroupNodeDeselectionEvent;

        float Rotation { get; set; }
        PointF Translation { get; set; }
        SizeF Scale { get; set; }

        [BrowsableAttribute(false)]
        bool Highlighted { get; set; }

        [BrowsableAttribute(false)]
        RectangleF Bounds { get; }

        bool Enabled { get; set; }

        void Adapt(ISvgType svg);
        void Draw(Graphics g);
                
        PointF[] GetBoundCorners();

        [BrowsableAttribute(false)]
        PointF Centre { get; }

        PointF GetTransformedCentre(Matrix m);

        void SpawnGroupNodeSelectionEvent(IGroupNode node);
        void SpawnGroupNodeDeselectionEvent(IGroupNode node);

        bool RemoveItem(IGroupNode node);

        IEnumerable<IShapeType> GetShapes();
        IEnumerable<IShapeType> RecursiveGetShapes();
        IEnumerable<IGroupNode> GetGnodes();
        IEnumerable<IGroupNode> GetGnodesOfClass(ClassType nodeClass);
        IEnumerable<IGroupNode> RecursiveGetGnodes();
        IEnumerable<IGroupNode> RecursiveGetGnodesOfClass(ClassType nodeClass);

        IEnumerable<IGroupNode> IncomingNodes();
        IEnumerable<IGroupNode> OutgoingNodes();

        bool IsOfClass(ClassType nodeClass, bool checkAncestors);

        [BrowsableAttribute(false)]
        bool HasShapes { get; }
        [BrowsableAttribute(false)]
        bool HasGnodes { get; }

        IGroupNode GetGnodeById(string id);
        IGroupNode GetGnodeByTitle(string title);

        IEnumerable<IGroupNode> IntersectsPoint(PointF p);

        [ReadOnlyAttribute(false)]
        IGroupNode Parent { get; set; }
        Matrix HeirarchyTransform { get; }
        Matrix LocalTransform { get; }

        object[] GetItems();
        string Transform();

        string Id { get; }
        ClassType Class { get; }
        string Title { get; }
        string Label { get; }
        string Description { get; }
        string ToolTipText { get; }

        float Proximity { get; }
    }
}
