﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using Graph.NET.Utils;

namespace Graph.NET
{
    public class ControlPointManager
    {
        #region Fields

        private Dictionary<ulong, ControlPoint> m_controlPoints = new Dictionary<ulong, ControlPoint>();

        #endregion

        #region Properties

        public IEnumerable<ControlPoint> ControlPoints
        {
            get { return m_controlPoints.Values.ToList(); }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds ControlPoints that will apply to the node.
        /// </summary>
        /// <param name="node">The node to add ControlPoints for.</param>
        public void AddControlPoints(IGroupNode node)
        {
            if (node.IsOfClass(ClassType.edge))
            {
                foreach (IShapeType shape in node.RecursiveGetShapes())
                {
                    foreach (ControlPoint cp in shape.CreateControlPoints())
                    {
                        if (!m_controlPoints.ContainsKey(cp.ID))
                        {
                            m_controlPoints.Add(cp.ID, cp);

                            node.GroupNodeSelectionEvent += new GroupNodeEventHandler(cp.GroupNodeSelectionHandler);
                            node.GroupNodeDeselectionEvent += new GroupNodeEventHandler(cp.GroupNodeDeselectionHandler);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Purges all control points.
        /// </summary>
        public void Clear()
        {
            foreach (ControlPoint cp in m_controlPoints.Values)
            {
                cp.Owner.Container.Enabled = true;
            }

            m_controlPoints.Clear();
        }

        /// <summary>
        /// Draws all enabled ControlPoints.
        /// </summary>
        /// <param name="g">The Graphics required to perform drawing.</param>
        /// <param name="m">The world matrix to transform the drawing.</param>
        public void Draw(Graphics g, Matrix m)
        {
            foreach (ControlPoint cp in m_controlPoints.Values.Where(cp => cp.Visible))
            {
                cp.Draw(g, m.Clone());
            }
        }

        /// <summary>
        /// Returns a collection of ControlPoints that intersect with the point p.
        /// </summary>
        /// <param name="p">The point to check</param>
        /// <param name="m">The transform to apply to the point when checking for intersection.</param>
        /// <returns>A Collection of ControlPoints that intersect with the point p.</returns>
        public IEnumerable<ControlPoint> IntersectsPoint(PointF p, Matrix m)
        {
            foreach (ControlPoint cp in m_controlPoints.Values.Where(cp => cp.Visible))
            {
                if (cp.IntersectsPoint(p, m.Clone()))
                {
                    yield return cp;
                }
            }
        }

        /// <summary>
        /// Returns a collection of all ControlPoints
        /// that are ControlPoints of the same IGroupNode.
        /// </summary>
        /// <param name="p">The ControlPoint to find relations to.</param>
        /// <returns>collection of all ControlPoints that are ControlPoints 
        /// of the same IGroupNode</returns>
        public IEnumerable<ControlPoint> GetRelatedControlPoints(ControlPoint p)
        {
            foreach (ControlPoint cp in m_controlPoints.Values.Where(cp => cp != p && cp.Owner.Container == p.Owner.Container))
            {
                yield return cp;
            }
        }

        /// <summary>
        /// Calls Animate on all visible ControlPoints.
        /// </summary>
        /// <param name="deltaTimeInSeconds">Timeslice in seconds.</param>
        public void Animate(double deltaTimeInSeconds)
        {
            foreach (ControlPoint cp in m_controlPoints.Values.Where(cp => cp.Visible))
            {
                cp.Animate(deltaTimeInSeconds);
            }
        }

        #endregion 
    }

    public class ControlPoint : AbstractDrawable
    {
        public enum ControlPointType
        {
            neither,
            head,
            tail
        }

        public enum ControlPointShape
        {
            square,
            circle
        }

        private static readonly float DEFAULT_SIZE = 10f;
        private static readonly Color INACTIVE_COLOR = Color.Red;
        private static readonly Color ACTIVE_COLOR = Color.Violet;

        #region Fields

        private float m_size = DEFAULT_SIZE;

        private PointF m_initialPosition;
        private PointF m_activePosition;

        private static ulong m_nextId = 0;

        private bool m_focussed = false;
        private bool m_active = false;
        
        private ulong m_id = m_nextId++;

        private ControlPointShape m_shape = ControlPointShape.circle;
        private ControlPointType m_type = ControlPointType.neither;
        
        private IShapeType m_owner = null;
        private IGroupNode m_target = null;

        /// <summary>
        /// Tracking animation time.
        /// </summary>
        private double m_timePassed = 0;

        #endregion

        #region Properties

        public bool Active
        {
            get { return m_active; }
            set
            {
                m_active = value;
                m_activePosition = m_initialPosition;
            }
        }

        public bool Focussed
        {
            get { return m_focussed; }
            set
            {
                m_focussed = value;

                if (m_focussed)
                {
                    Thickness = 2;
                    LineColor = ACTIVE_COLOR;
                }
                else
                {
                    Thickness = 1;
                    LineColor = INACTIVE_COLOR;
                }
            }
        }

        public IShapeType Owner
        {
            get { return m_owner; }
        }

        public IGroupNode Target
        {
            get { return m_target; }
        }

        public ulong ID
        {
            get { return m_id; }
        }

        public ControlPointShape Shape
        {
            get { return m_shape; }
            set { m_shape = value; }
        }

        public ControlPointType Type
        {
            get { return m_type; }
        }

        public PointF Position
        {
            get
            {
                if (Active)
                {
                    return m_activePosition;
                }
                else
                {
                    return m_initialPosition;
                }
            }
            set
            {
                if (Active)
                {
                    m_activePosition = value;
                }
                else
                {
                    m_initialPosition = value;
                }

                m_rectangle.Location = new PointF(
                    Position.X - m_size / 2F, 
                    Position.Y - m_size / 2F);
            }
        }

        public float Size
        {
            get { return m_size; }
            set
            { 
                m_size = value;
                m_rectangle.Size = new SizeF(m_size, m_size);
            }
        }

        #endregion

        #region Constructors

        public ControlPoint(PointF point, IShapeType owner, ControlPointType type)
        {
            m_owner = owner;
            Position = point;
            m_type = type;

            LineColor = INACTIVE_COLOR;

            m_rectangle.Size = new SizeF(m_size, m_size);

            switch (m_type)
            {
                case ControlPointType.head:
                    m_target = m_owner.Container.OutgoingNodes().FirstOrDefault();
                    break;
                case ControlPointType.tail:
                    m_target = m_owner.Container.IncomingNodes().FirstOrDefault();
                    break;
                default:
                    break;
            }
        }

        private ControlPoint() { /* Not Allowed */ }

        #endregion

        #region Public Methods

        public bool IntersectsPoint(PointF point, Matrix m)
        {
            if (Visible)
            {
                return GetTransformedRect(m).Contains(point);
            }

            return false;
        }

        public override void Animate(double deltaTimeInSeconds)
        {
            if (Focussed)
            {
                m_timePassed += deltaTimeInSeconds;

                double t = Math.Sin(m_timePassed * 2.0 * Math.PI * 2.0);

                Size = DEFAULT_SIZE + (float)(0.8 * t);                
            }

            Shape = m_active ? ControlPointShape.circle : ControlPointShape.square;
        }

        public void Draw(Graphics g, Matrix m)
        {
            RectangleF rect = GetTransformedRect(m);

            if (Visible)
            {
                GraphicsContainer c = g.BeginContainer();
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;

                    g.Transform.Multiply(Transform);

                    using (Pen pen = new Pen(LineColor, Thickness))
                    {
                        switch (m_shape)
                        {
                            case ControlPointShape.square:
                                g.DrawRectangle(pen, Rectangle.Round(rect));
                                break;
                            case ControlPointShape.circle:
                                g.DrawEllipse(pen, Rectangle.Round(rect));
                                break;
                        }
                    }

                } g.EndContainer(c);
            }
        }

        #endregion

        #region private Helpers

        private RectangleF GetTransformedRect(Matrix m)
        {
            PointF p = Position;

            // Only apply transform when control point is inactive.
            // An active ControlPoint directly from the cursor position.
            if (!Active)
            {
                m.Multiply(Owner.Container.HeirarchyTransform);

                p = MathUtils.TransformPoint(m, Position);
            }

            return new RectangleF(new PointF(
                p.X - m_size / 2F, p.Y - m_size / 2F), new SizeF(m_size, m_size));
        }

        #endregion

        #region Event Handlers
        public void GroupNodeSelectionHandler(object sender, GroupNodeEventArgs e)
        {
            Visible = true;
        }

        public void GroupNodeDeselectionHandler(object sender, GroupNodeEventArgs e)
        {
        }
        #endregion

        #region Static Helpers
        public static PointF[] PointFArray(ControlPoint[] controlPoints)
        {
            PointF[] points = new PointF[controlPoints.Length];

            for (int i = 0; i < controlPoints.Length; ++i)
            {
                points[i] = controlPoints[i].Position;
            }

            return points;
        }
        #endregion        
    }

    public delegate void ControlPointEditHandler(object sender, ControlPointEditEventArgs args);

    public class ControlPointEditEventArgs : EventArgs
    {
        private IGroupNode m_target = null;
        private ControlPoint m_controlPoint = null;

        public ControlPoint ControlPoint
        {
            get { return m_controlPoint; }
        }

        public IGroupNode Target
        {
            get { return m_target; }
        }

        public ControlPointEditEventArgs(ControlPoint controlPoint, IGroupNode target)
        {
            m_controlPoint = controlPoint;
            m_target = target;
        }
    }
}
