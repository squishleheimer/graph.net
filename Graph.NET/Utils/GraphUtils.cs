﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Graph.NET.GraphControls;
using System.Threading.Tasks;

namespace Graph.NET.Utils
{
    /// <summary>
    /// Static Helper Methods for parsing the svg structure for node-edge
    /// relationships. Graph Nodes have associated incoming and outgoing
    /// edges and other nodes.
    /// </summary>
    public static class GraphUtils
    {
        /// <summary>
        /// Helper method to asynchronously load a graph using graphviz exe.
        /// </summary>
        /// <param name="dot">The dot cmd txt.</param>
        /// <param name="panel">The GraphContainerPanel</param>
        /// <param name="source">CancellationTokenSource that provides a cancellation mechanism.</param>
        /// <param name="ppMethod">An optional post-processing method to allow modification of the resultant svg xml.</param>
        /// <param name="postLoadAction">An optional action to allow code after loading.</param>
        public static void LoadGraph(
            string dot,
            GraphContainerPanel panel,
            CancellationTokenSource source,
            SvgExporter.PostProcessSvgMarkup ppMethod = null,
            Action postLoadAction = null)
        {
            new SvgExporter(dot).CreateSvgTypeUsingDotExecutableAsync(source.Token, ppMethod).ContinueWith((x) =>
            {
                if (panel.InvokeRequired)
                {
                    panel.BeginInvoke((Action)(() =>
                    {
                        panel.Svg = x.Result;
                    }));
                }
                else
                {
                    panel.Svg = x.Result;
                }

                if (postLoadAction != null)
                {
                    postLoadAction();
                }

            }, source.Token, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public static bool RemoveGraphNode(ISvgType svg, IGroupNode node)
        {
            bool removed = false;

            foreach (IGroupNode gnode in svg.GetGnodes())
            {
                removed = gnode.RemoveItem(node);

                if (removed)
                {
                    RemoveAssociatedEdges(svg, node);

                    RemoveAssociatedJoinerNodes(svg, node);
                }
            }

            return removed;
        }

        private static void RemoveAssociatedEdges(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode gnode in svg.GetGnodes())
            {
                List<IGroupNode> edges = new List<IGroupNode>(gnode.GetGnodesOfClass(ClassType.edge));

                int index = 0;

                while (index < edges.Count)
                {
                    IGroupNode edge = edges[index];

                    string[] nodeNames = Regex.Split(edge.Title, @"->|--");

                    bool removed = false;

                    foreach (string name in nodeNames)
                    {
                        if (name == node.Title)
                        {
                            removed = gnode.RemoveItem(edge);
                        }
                    }

                    if (!removed)
                    {
                        ++index;
                    }
                }
            }
        }

        private static void RemoveAssociatedJoinerNodes(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode gnode in svg.GetGnodes())
            {
                List<IGroupNode> joinerNodes = new List<IGroupNode>(gnode.GetGnodesOfClass(ClassType.node));

                foreach (IGroupNode joinerNode in joinerNodes)
                {
                    string[] nodeNames = Regex.Split(joinerNode.Title, @":");

                    foreach (string name in nodeNames)
                    {
                        if (name == node.Title)
                        {
                            RemoveGraphNode(svg, joinerNode);
                        }
                    }
                }
            }
        }

        public static IEnumerable<IGroupNode> GetIncomingEdges(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode edge in svg.RecursiveGetGnodesOfClass(ClassType.edge))
            {
                string[] nodeNames = Regex.Split(edge.Title, @"->|--");

                if (nodeNames.Length == 2 && nodeNames[1].Trim() == node.Title)
                {
                    yield return edge;
                }
            }
        }

        public static IEnumerable<IGroupNode> GetOutgoingEdges(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode edge in svg.RecursiveGetGnodesOfClass(ClassType.edge))
            {
                string[] nodeNames = Regex.Split(edge.Title, @"->|--");

                if (nodeNames.Length == 2 && nodeNames[0].Trim() == node.Title)
                {
                    yield return edge;
                }
            }
        }

        private static IGroupNode GetGnodeWithNameLike(string name, ISvgType svg)
        {
            IGroupNode gnode = null;

            const char SEPARATOR = ':';

            if (name.Contains(SEPARATOR))
            {
                if (name.Split(SEPARATOR).Any(n => svg.GetGnodeByTitle(n) != null))
                {
                    gnode = svg.GetGnodeByTitle(name.Split(SEPARATOR).FirstOrDefault(n => svg.GetGnodeByTitle(n) != null));
                }
            }
            else
            {
                gnode = svg.GetGnodeByTitle(name);
            }

            return gnode;
        }

        public static IEnumerable<IGroupNode> GetDestinationNode(ISvgType svg, IGroupNode edge)
        {
            Trace.Assert(edge.IsOfClass(ClassType.edge, false));

            string[] nodeNames = Regex.Split(edge.Title, @"->|--");

            IGroupNode destNode = svg.RecursiveGetGnodesOfClass(ClassType.node).FirstOrDefault(node => node.Title.Equals(nodeNames[1]));

            if (destNode != null)
            {
                yield return destNode;
            }
        }

        public static IEnumerable<IGroupNode> GetSourceNode(ISvgType svg, IGroupNode edge)
        {
            Trace.Assert(edge.IsOfClass(ClassType.edge, false));

            string[] nodeNames = Regex.Split(edge.Title, @"->|--");

            IGroupNode sourceNode = svg.RecursiveGetGnodesOfClass(ClassType.node).FirstOrDefault(node => node.Title.Equals(nodeNames[0]));

            if (sourceNode != null)
            {
                yield return sourceNode;
            }
        }

        public static IEnumerable<IGroupNode> GetIncomingNodes(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode edge in GetIncomingEdges(svg, node))
            {
                string[] nodeNames = Regex.Split(edge.Title, @"->|--");

                if (nodeNames.Length == 2 && nodeNames[1].Contains(node.Title))
                {
                    IGroupNode gnode = GetGnodeWithNameLike(nodeNames[0], svg);

                    if (gnode != null)
                    {
                        yield return gnode;
                    }
                }
            }
        }

        public static IEnumerable<IGroupNode> GetOutgoingNodes(ISvgType svg, IGroupNode node)
        {
            foreach (IGroupNode edge in GetOutgoingEdges(svg, node))
            {
                string[] nodeNames = Regex.Split(edge.Title, @"->|--");

                if (nodeNames.Length == 2 && nodeNames[0].Contains(node.Title))
                {
                    IGroupNode gnode = GetGnodeWithNameLike(nodeNames[1], svg);

                    if (gnode != null)
                    {
                        yield return gnode;
                    }
                }
            }
        }

        public static void GetAllGroupNodesInTraversal(ISvgType svg, IGroupNode node, List<IGroupNode> nodes)
        {
            foreach (IGroupNode gnode in GetIncomingNodes(svg, node))
            {
                if (!nodes.Contains(gnode))
                {
                    nodes.Add(gnode);

                    GetAllGroupNodesInTraversal(svg, gnode, nodes);
                }
            }

            foreach (IGroupNode gnode in GetIncomingEdges(svg, node))
            {
                if (!nodes.Contains(gnode))
                {
                    nodes.Add(gnode);

                    GetAllGroupNodesInTraversal(svg, gnode, nodes);
                }
            }
        }

        /// <summary>
        /// Checks whether the IGroupNode is not null and is of the ClassType
        /// passed in. If the node is not of the ClassType it could be null
        /// so do not try to call methods on it if this method returns false.
        /// Also, bitwise OR'd ClassType params may be used as boolean OR to
        /// pass in and see if the node is any of ClassTypes OR'd together.
        /// </summary>
        /// <param name="g">The IGroupNode that could be null!</param>
        /// <param name="classType">The ClassType(s) to check. You can use a bitwise OR'd
        /// parameter here and achieve the regular boolean equivalent to see if
        /// the classType of g is any of the bitwise OR'd enums.</param>
        /// <returns>True if the node is not null AND is any of the ClassTypes
        /// defined by the bitwise OR'd classType param.
        /// </returns>
        /// <remarks>Basically this method allows us to avoid checking a group node
        /// against null before then trying to check its ClassType.</remarks>
        internal static bool IsOfClass(this IGroupNode g, ClassType classType)
        {
            return g != null && Enum.GetValues(typeof(ClassType)).Cast<ClassType>().Any(c => g.IsOfClass(classType & c, false));
        }
    }
}
