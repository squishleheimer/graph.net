﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;

namespace Graph.NET.Utils
{
    public static class MathUtils
    {
        #region Static Helpers

        public static double GeneratePulseFromZeroToRange(double min, double max, double frequency)
        {
            double amp = (max - min);
            return min + (0.5 * amp * (1.0 + (float)Math.Cos(frequency)));
        }

        public static double DistanceBetweenPoints(PointF a, PointF b)
        {
            PointF diff = new PointF(a.X - b.X, a.Y - b.Y);

            return Math.Sqrt(diff.X * diff.X + diff.Y * diff.Y);
        }

        public static bool IsPointWithinDistance(PointF a, PointF b, double d)
        {
            return DistanceBetweenPoints(a, b) <= d;
        }

        public static bool IsPointWithinDistanceOfOneOfPoints(PointF a, PointF[] points, double d)
        {
            foreach (PointF b in points)
            {
                if (IsPointWithinDistance(a,b,d))
                {
                    return true;
                }
            }

            return false;
        }

        public static PointF[] GetRectangleCorners(RectangleF rect)
        {
            PointF[] points = new PointF[4];

            points[0] = rect.Location;
            points[1] = PointF.Add(rect.Location, new SizeF(rect.Width, 0));
            points[2] = PointF.Add(rect.Location, rect.Size);
            points[3] = PointF.Add(rect.Location, new SizeF(0, rect.Height));

            return points;
        }

        public static PointF GetRectangleCentre(RectangleF rect)
        {
            return PointF.Add(rect.Location, 
                new SizeF(rect.Size.Width / 2f, rect.Size.Height / 2f));
        }        

        public static PointF TransformPoint(Matrix m, PointF p)
        {
            PointF[] points = new PointF[] { p };

            m.TransformPoints(points);

            return points[0];
        }

        public static void TransformPoint(Matrix m, ref PointF p)
        {
            p = TransformPoint(m, p);
        }

        public static Graph.NET.pathType.PathPoint GetClosestPathPoint(PointF p, pathType path)
        {
            Graph.NET.pathType.PathPoint closest = path.PathPoints[0];
            double d = DistanceBetweenPoints(p, closest.Point);

            for (int i = 1; i < path.PathPoints.Count; ++i)
            {
                double dist = DistanceBetweenPoints(p, path.PathPoints[i].Point);

                if (dist < d)
                {
                    d = dist;
                    closest = path.PathPoints[i];
                }
            }

            return closest;
        }

        public static PointF TangentPointOnLine(PointF point, PointF start, PointF end)
        {
            double length = DistanceBetweenPoints(end, start);            

            double U = (((point.X - start.X) * (end.X - start.X)) 
                + ((point.Y - start.Y) * (end.Y - start.Y))) / (length * length);

            if (U < 0.0f)
            {
                U = 0.0f;
            }
            else if (U > 1.0f)
            {
                U = 1.0f;
            }

            PointF intersection = new PointF(
                (float)(start.X + U * (end.X - start.X)), 
                (float)(start.Y + U * (end.Y - start.Y)));

            return intersection;
        }

        #endregion
    }

    /// <summary>
    /// Represents a 2D vector
    /// </summary>
    public class Vector : ICloneable
    {
        #region Private members and properties

        private float m_X = 0;
        private float m_Y = 0;

        /// <summary>
        /// X Coordination of vector
        /// </summary>
        public float X
        {
            get { return m_X; }
            set { m_X = value; }
        }

        /// <summary>
        /// Y Coordination of vector
        /// </summary>
        public float Y
        {
            get { return m_Y; }
            set { m_Y = value; }
        }

        /// <summary>
        /// Gets the length of vector.
        /// </summary>
        /// <value>The length.</value>
        public float Length
        {
            get { return (float)Math.Sqrt(X * X + Y * Y); }
        }

        /// <summary>
        /// Gets the squared length of vector.
        /// </summary>
        /// <value>The squared length.</value>
        public float LengthSq
        {
            get { return X * X + Y * Y; }
        }

        /// <summary>
        /// Gets the angle (in radians) between x-axis and vector's projection to OXY plane.
        /// </summary>
        /// <value>The angle.</value>
        public float Theta
        {
            get { return (float)Math.Atan2(Y, X); }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor. Initiate vector at the (0,0,0) location
        /// </summary>
        public Vector() { }

        /// <summary>
        /// Initiate 2D vector with given parameters
        /// </summary>
        /// <param name="x">X coordination of vector</param>
        /// <param name="y">Y coordination of vector</param>
        public Vector(float x, float y)
        {
            m_X = x;
            m_Y = y;
        }

        /// <summary>
        /// Initiate vector with same values as given Vector
        /// </summary>
        /// <param name="vector">Vector to copy coordinations</param>
        public Vector(Vector v)
        {
            m_X = v.X;
            m_Y = v.Y;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates a PointF from the X, Y components.
        /// </summary>
        /// <param name="v">The vector to convert.</param>
        /// <returns>The PointF</returns>
        public static PointF ToPointF(Vector v)
        {
            return v.ToPointF();
        }

        /// <summary>
        /// Creates a PointF from the X, Y components.
        /// </summary>
        /// <returns>The PointF</returns>
        public PointF ToPointF()
        {
            return new PointF(X, Y);
        }

        /// <summary>
        /// Creates a position Vector  from a PointF
        /// </summary>
        /// <param name="p">The point.</param>
        /// <returns>A position Vector  from a PointF.</returns>
        public static Vector FromPointF(PointF p)
        {
            return new Vector(p.X, p.Y);
        }

        /// <summary>
        /// Add 2 vectors and create a new one.
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>New vector that is the sum of the 2 vectors</returns>
        public static Vector Add(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        /// <summary>
        /// Subtract 2 vectors and create a new one.
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>New vector that is the difference of the 2 vectors</returns>
        public static Vector Subtract(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        /// <summary>
        /// Return a new vector with negative values.
        /// </summary>
        /// <param name="vector">Original vector</param>
        /// <returns>New vector that is the inversion of the original vector</returns>
        public static Vector Negate(Vector v)
        {
            return new Vector(-v.X, -v.Y);
        }

        /// <summary>
        /// Multiply a vector with a scalar
        /// </summary>
        /// <param name="vector">Vector to be multiplied</param>
        /// <param name="val">Scalar to multiply vector</param>
        /// <returns>New vector that is the multiplication of the vector with the scalar</returns>
        public static Vector Multiply(Vector v, float val)
        {
            return new Vector(v.X * val, v.Y * val);
        }

        /// <summary>
        /// Calculates dot product of n vectors.
        /// </summary>
        /// <param name="vectors">vectors array.</param>
        /// <returns></returns>
        public static float DotProduct(params Vector[] vectors)
        {
            if (vectors.Length < 2)
            {
                throw new ArgumentException("dot product can be calculated from at least two vectors");
            }

            float dx = vectors[0].X, dy = vectors[0].Y;

            for (int i = 1; i < vectors.Length; i++)
            {
                dx *= vectors[i].X;
                dy *= vectors[i].Y;
            }

            return (dx + dy);
        }

        public static Vector Contract(Vector v, float dLength)
        {
            float length = v.Length;

            if (length == 0)
            {
                throw new ArgumentException("Vector length equals zero. Can't contract or expand.");
            }

            return new Vector(v.X - (v.X * dLength / length),
                              v.Y - (v.Y * dLength / length));
        }

        public static Vector Expand(Vector v, float dLength)
        {
            return Contract(v, -1 * dLength);
        }

        public void Translate(float dx, float dy)
        {
            X += dx;
            Y += dy;
        }

        public Vector Normalised()
        {
            float length = this.Length;

            if (length == 0)
            {
                throw new ArgumentException("Vector length equals zero. Can't normalise.");
            }

            return new Vector(this.X / length,
                              this.Y / length);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Check equality of two vectors
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>True - if he 2 vectors are equal.
        /// False - otherwise</returns>
        public static bool operator ==(Vector a, Vector b)
        {
            return ((a.X.Equals(b.X))
                    && (a.Y.Equals(b.Y)));
        }

        /// <summary>
        /// Check inequality of two vectors
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>True - if he 2 vectors are not equal.
        /// False - otherwise</returns>
        public static bool operator !=(Vector a, Vector b)
        {
            return (!(a == b));
        }

        /// <summary>
        /// Calculate the sum of 2 vectors.
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>New vector that is the sum of the 2 vectors</returns>
        public static Vector operator +(Vector a, Vector b)
        {
            return Add(a, b);
        }

        /// <summary>
        /// Calculate the subtraction of 2 vectors
        /// </summary>
        /// <param name="vector1">First vector</param>
        /// <param name="vector2">Second vector</param>
        /// <returns>New vector that is the difference of the 2 vectors</returns>
        public static Vector operator -(Vector a, Vector b)
        {
            return Subtract(a, b);
        }

        public static Vector operator -(Vector v, float dLength)
        {
            return Contract(v, dLength);
        }

        public static Vector operator +(Vector v, float dLength)
        {
            return Expand(v, dLength);
        }

        /// <summary>
        /// Calculate the negative (inverted) vector
        /// </summary>
        /// <param name="vector">Original vector</param>
        /// <returns>New vector that is the invertion of the original vector</returns>
        public static Vector operator -(Vector v)
        {
            return Negate(v);
        }

        /// <summary>
        /// Calculate the multiplication of a vector with a scalar
        /// </summary>
        /// <param name="vector">Vector to be multiplied</param>
        /// <param name="val">Scalar to multiply vector</param>
        /// <returns>New vector that is the multiplication of the vector and the scalar</returns>
        public static Vector operator *(Vector v, float val)
        {
            return Multiply(v, val);
        }

        /// <summary>
        /// Calculate the multiplication of a vector with a scalar
        /// </summary>
        /// <param name="val">Scalar to multiply vector</param>
        /// <param name="vector">Vector to be multiplied</param>
        /// <returns>New vector that is the multiplication of the vector and the scalar</returns>
        public static Vector operator *(float val, Vector v)
        {
            return Multiply(v, val);
        }

        public float this[byte index]
        {
            get
            {
                if (index < 0 || index > 1) throw new ArgumentException("index has to be integer from interval [0, 1]");
                switch (index)
                {
                    case 0:
                        return X;
                    case 1:
                        return Y;
                    default:
                        return 0;
                }
            }
            set
            {
                if (index < 0 || index > 1) throw new ArgumentException("index has to be integer from interval [0, 1]");
                switch (index)
                {
                    case 0:
                        X = value;
                        break;
                    case 1:
                        Y = value;
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion

        #region Constants

        /// <summary>
        /// Standard (0,0,0) vector
        /// </summary>
        public static Vector Zero
        {
            get { return new Vector(0.0f, 0.0f); }
        }

        /// <summary>
        /// Standard (1,0,0) vector
        /// </summary>
        public static Vector XAxis
        {
            get { return new Vector(1.0f, 0.0f); }
        }

        /// <summary>
        /// Standard (0,1,0) vector
        /// </summary>
        public static Vector YAxis
        {
            get { return new Vector(0.0f, 1.0f); }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"></see> to compare with the current <see cref="T:System.Object"></see>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"></see> is equal to the current <see cref="T:System.Object"></see>; otherwise, false.
        /// </returns>
        public override bool Equals(object obj)
        {
            return (obj is Vector && (Vector)obj == this);
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "({0}, {1})", m_X, m_Y);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override int GetHashCode()
        {
            return m_X.GetHashCode() ^ m_Y.GetHashCode();
        }

        #endregion

        #region ICloneable Members

        public object Clone()
        {
            return new Vector(this);
        }

        #endregion
    }

    public class Ellipse
    {
        private PointF m_f1 = PointF.Empty;
        private PointF m_f2 = PointF.Empty;
        private float m_xRadius = 0f;
        private float m_yRadius = 0f;

        public float MajorAxis
        {
            get { return m_xRadius >= m_yRadius ? m_xRadius : m_yRadius; }
        }

        public float MinorAxis
        {
            get { return m_xRadius < m_yRadius ? m_xRadius : m_yRadius; }
        }

        public RectangleF Rect
        {
            get 
            {
                SizeF size = new SizeF(m_xRadius * 2f, m_yRadius * 2f);

                return new RectangleF(
                    -size.Width / 2f,
                    -size.Height / 2f,
                    size.Width,
                    size.Height);
            }
        }

        public Ellipse(float xRadius, float yRadius)
        {
            Init(xRadius, yRadius);
        }

        public Ellipse(SizeF size)
        {
            Init(size.Width / 2f, size.Height / 2f);
        }

        private void Init(float xRadius, float yRadius)
        {
            m_xRadius = xRadius;
            m_yRadius = yRadius;

            float majorAxis = MajorAxis;
            float minorAxis = MinorAxis;

            float F = (float)Math.Sqrt(majorAxis * majorAxis - minorAxis * minorAxis);

            if (m_xRadius >= m_yRadius)
            {
                m_f1 = new PointF(-F, 0);
                m_f2 = new PointF( F, 0);
            }
            else
            {
                m_f1 = new PointF(0, F);
                m_f2 = new PointF(0,-F);
            }
        }

        public PointF PointOnCircumference(float angle)
        {
            return new PointF(
                (float)Math.Cos(angle) * m_xRadius,
                (float)Math.Sin(angle) * m_yRadius);
        }

        public Vector DetermineEllipseTangent(PointF point)
        {
            return new Vector(
                -(m_xRadius * point.Y) / m_yRadius,
                 (m_yRadius * point.X) / m_xRadius).Normalised();
        }   
    }
}
