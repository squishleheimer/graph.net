﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Graph.NET.Utils
{
    public class GraphBuilder
    {
        public enum GraphType
        {
            digraph,
            graph
        }

        private static long m_nextNodeId = 0;
        private static long m_nextEdgeId = 0;

        public GraphType Type { get; set; }

        public ISvgType Svg { get; private set; }

        public string GraphTitle
        {
            get { return Svg != null ? Svg.Title : Guid.NewGuid().ToString(); }
        }

        public GraphBuilder(ISvgType svg = null, GraphType type = GraphType.digraph)
        {
            Svg = svg;
            Type = type;

            m_nextNodeId = 0;
            m_nextEdgeId = 0;
        }

        public override string ToString()
        {
            return String.Format(GraphFormat, GraphTitle, _ToString());
        }

        private long NextNodeID
        { 
            get { return NextID(ref m_nextNodeId, ClassType.node); } 
        }

        private long NextEdgeID
        {
            get { return NextID(ref m_nextEdgeId, ClassType.edge); }
        }

        private long NextID(ref long _id, ClassType nodeType)
        {
            if (Svg != null)
            {
                long id = 0;

                foreach (IGroupNode g in Svg.RecursiveGetGnodesOfClass(nodeType))
                {
                    long lastId = 0;
                    if (long.TryParse(g.Id.Replace(nodeType.ToString(), String.Empty), out lastId) && lastId > id)
                    {
                        id = lastId;
                    }
                }

                _id = id + 1;
            }
            else
            {
                ++_id;
            }

            return _id;
        }

        private string GraphFormat
        {
            get { return String.Format(@"{0} ""{{0}}"" {{{{{1}{{1}}}}}}", Type, Environment.NewLine); }
        }

        public static string NodeFormat
        {
            get { return String.Format(@"""{{0}}"" [id=""{{1}}"" label=""{{2}}""];"); }
        }

        public static string EdgeFormat
        {
            get { return String.Format(@"""{{0}}""->""{{1}}"" [id=""{{2}}"" label=""{{3}}""];");  }
        }

        private string NewNodeID
        {
            get { return NewID(NextNodeID, ClassType.node); }
        }

        private string NewEdgeID
        {
            get { return NewID(NextEdgeID, ClassType.edge); }
        }

        private string NewID(long id, ClassType nodeType)
        {
            return String.Format(@"{0}{1}", nodeType, id, CultureInfo.InvariantCulture);
        }

        public StringBuilder BeginBuilder()
        {
            return _ToString();
        }

        public StringBuilder AddNodeToBuilder(StringBuilder builder, out string id, string label = null)
        {
            id = NewNodeID;

            if (label == null)
            {
                label = id;
            }

            return builder.AppendNode(id, label);
        }

        public StringBuilder AddEdgeToBuilder(StringBuilder builder, out string newEdgeId, string label = null, string sourceId = null, string destinationId = null)
        {
            newEdgeId = NewEdgeID;

            if (label == null)
            {
                label = newEdgeId;
            }

            return builder.AppendEdge(newEdgeId, label, sourceId, destinationId);
        }

        public StringBuilder RemoveEdgeFromBuilder(StringBuilder builder, string id, string label = null, string sourceId = null, string destinationId = null)
        {
            if (label == null)
            {
                label = id;
            }
            
            return builder.Replace(String.Format(GraphBuilder.EdgeFormat, sourceId, destinationId, id, label), String.Empty);
        }

        public string EndBuilder(StringBuilder builder)
        {
            return String.Format(GraphFormat, GraphTitle, builder);
        }

        private StringBuilder _ToString()
        {
            var builder = new StringBuilder();

            if (Svg != null)
            {
                var nodes = Svg.RecursiveGetGnodesOfClass(ClassType.node).ToList();

                nodes.ForEach(n =>
                {
                    builder.AppendNode(n.Id, n.Label);
                });

                nodes.ForEach(n =>
                {
                    GraphUtils.GetOutgoingEdges(Svg, n).ToList().ForEach(e =>
                    {
                        e.OutgoingNodes().ToList().ForEach(m =>
                        {
                            builder.AppendEdge(e.Id, e.Label, n.Id, m.Id);
                        });
                    });
                });
            }

            return builder;
        }
    }

    public static class StringBuilderExt
    {
        public static StringBuilder AppendNode(this StringBuilder sb, string id, string label = null)
        {
            return sb.AppendLine(String.Format(GraphBuilder.NodeFormat, id, id, label));
        }

        public static StringBuilder AppendEdge(this StringBuilder sb, string id, string label = null, string sourceId = null, string destinationId = null)
        {
            if (!String.IsNullOrWhiteSpace(sourceId) && !String.IsNullOrWhiteSpace(destinationId))
            {
                return sb.AppendLine(String.Format(GraphBuilder.EdgeFormat, sourceId, destinationId, id, label));
            }

            return sb;
        }
    }
}
