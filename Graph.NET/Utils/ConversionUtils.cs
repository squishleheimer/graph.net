﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;

namespace Graph.NET.Utils
{
    public static class ConversionUtils
    {
        private static Point ParsePoint(string point)
        {
            return Point.Round(ParsePointF(point));
        }

        public static PointF ParsePointF(string point)
        {
            string[] split = Regex.Split(point, @"[\s]+|,");

            Trace.Assert(split.Length == 2);

            float x = float.Parse(split[0], CultureInfo.InvariantCulture);
            float y = float.Parse(split[1], CultureInfo.InvariantCulture);

            return new PointF(x, y);
        }

        public static PointF[] ParsePointFs(string pointsStr)
        {
            Trace.Assert(pointsStr != null);

            string[] points = Regex.Split(pointsStr.Trim(), " ");

            if (points.Length > 0)
            {
                PointF[] pointArray = new PointF[points.Length];

                int index = 0;
                foreach (string point in points)
                {
                    if (!String.IsNullOrEmpty(point))
                    {
                        pointArray[index] = ParsePointF(point);
                        ++index;
                    }
                }

                return pointArray;
            }

            return null;
        }

        public static StringDictionary ParseStyle(string styleStr)
        {
            Trace.Assert(styleStr != null);

            string[] styles = Regex.Split(styleStr.Trim(), ";");

            if (styles.Length > 0)
            {
                StringDictionary styleMap = new StringDictionary();

                foreach (string style in styles)
                {
                    string[] keyValue = Regex.Split(style, ":");

                    Trace.Assert(keyValue.Length > 0);

                    if (!styleMap.ContainsKey(keyValue[0]) && !String.IsNullOrEmpty(keyValue[0]))
                    {
                        styleMap.Add(keyValue[0], keyValue[1]);
                    }
                }

                return styleMap;
            }

            return null;
        }

        public static PointF ParsePathPointString(string p, char[] trimChars, ref char type)
        {
            foreach (char c in trimChars)
            {
                string cStr = new string(new char[] { c });

                if (p.Contains(cStr))
                {
                    type = c;
                    break;
                }
            }

            string trimStr = p.Trim(trimChars);

            PointF point = ParsePointF(trimStr);

            return point;
        }

        public static Color GetColor(string c, Color defaultColour)
        {
            Color colour = defaultColour;

            if (!String.IsNullOrWhiteSpace(c) && String.Compare(c, "none", true) != 0 && !c.StartsWith("url"))
            {
                c = c.ToLower();

                if (c.Contains("#"))
                {
                    string colorStr = c.TrimStart("#".ToCharArray());

                    int r = Convert.ToByte(colorStr.Substring(0, 2), 16);
                    int g = Convert.ToByte(colorStr.Substring(2, 2), 16);
                    int b = Convert.ToByte(colorStr.Substring(4, 2), 16);

                    return Color.FromArgb(r, g, b);
                }
                else if (c.Contains("grey"))
                {
                    c = c.Replace("grey", "gray");
                }

                colour = Color.FromName(c);
            }

            return colour;
        }

        public static Color ColorComplement(Color colour)
        {
            Color complement = new Color();

            byte r = Convert.ToByte(255 - colour.R);
            byte g = Convert.ToByte(255 - colour.G);
            byte b = Convert.ToByte(255 - colour.B);

            complement = Color.FromArgb(colour.A, r, g, b);

            return complement;
        }

        public static SizeF ParseDimensions(string width, string height, GraphicsUnit units)
        {
            const string regex = @"(\-?\d*\.?\d+)";

            string[] widthStr = TrimStringArray(Regex.Split(width, regex)).ToArray();
            string[] heightStr = TrimStringArray(Regex.Split(height, regex)).ToArray();

            string measurement = "px";

            if (widthStr.Length == 2)
            {
                measurement = widthStr[1];
            }

            float w = float.Parse(widthStr[0], CultureInfo.InvariantCulture);
            float h = float.Parse(heightStr[0], CultureInfo.InvariantCulture);

            if (measurement == "in")
            {
                units = GraphicsUnit.Inch;
            }
            if (measurement == "px")
            {
                units = GraphicsUnit.Pixel;
            }
            if (measurement == "pt")
            {
                units = GraphicsUnit.Point;
            }
            if (measurement == "%")
            {
                units = GraphicsUnit.World;
            }

            return new SizeF(w, h);
        }

        public static IEnumerable<string> TrimStringArray(string[] list)
        {
            foreach (var item in list.Where(s => !String.IsNullOrEmpty(s)))
            {
                yield return item;
            }
        }

        public static SizeF CalcReciprocal(Graphics g)
        {
            switch (g.PageUnit)
            {
                case GraphicsUnit.World:
                case GraphicsUnit.Pixel:
                    return new SizeF(1f, 1f);
                case GraphicsUnit.Inch:
                    return new SizeF(1f / g.DpiX, 1f / g.DpiY);
                case GraphicsUnit.Millimeter:
                    return new SizeF(25.4f / g.DpiX, 25.4f / g.DpiY);
                case GraphicsUnit.Point:
                    return new SizeF(72f / g.DpiX, 72f / g.DpiY);
                case GraphicsUnit.Display:
                    return new SizeF(75f / g.DpiX, 75f / g.DpiY);
                case GraphicsUnit.Document:
                    return new SizeF(300f / g.DpiX, 300f / g.DpiY);
            }

            return SizeF.Empty;
        }

        public static RectangleF RectangleFFromPoints(PointF[] points)
        {
            float xMin = float.MaxValue;
            float yMin = float.MaxValue;
            float xMax = float.MinValue;
            float yMax = float.MinValue;

            foreach (PointF p in points)
            {
                if (p.X < xMin)
                {
                    xMin = p.X;
                }

                if (p.Y < yMin)
                {
                    yMin = p.Y;
                }

                if (p.X > xMax)
                {
                    xMax = p.X;
                }

                if (p.Y > yMax)
                {
                    yMax = p.Y;
                }
            }

            return new RectangleF(
                xMin,
                yMin,
                xMax - xMin,
                yMax - yMin);
        }
    }
}
