﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Graph.NET.Utils
{
    namespace GraphicsExtensions
    {
        public static class ArrowRenderer
        {
            public const float TO_DEGREES = (float)(180.0 / Math.PI);

            public static void DrawStraightArrow(
                this Graphics g,
                Pen shaftStroke,
                Pen headStroke,
                Brush headFill,
                PointF a, PointF b,
                float headWidth,
                float headLength)
            {
                Vector A = Vector.FromPointF(a);
                Vector B = Vector.FromPointF(b);
                Vector AB = B - A;

                if (0 < AB.Length)
                {
                    // Where the shaft joins the pile.
                    Vector pileJoin = A + Vector.Contract(AB, headLength);

                    g.DrawLine(shaftStroke, a, pileJoin.ToPointF());
                    g.DrawArrowHead(headStroke, headFill,
                        CreateArrowHead(pileJoin, AB.Normalised(), headWidth, headLength));
                }
            }

            public static void DrawLoopArrow(
                this Graphics g,
                Pen shaftStroke,
                Pen headStroke,
                Brush headFill,
                PointF target,
                float headWidth,
                float headLength,
                float xRadius,
                float yRadius,
                float startAngle,
                float sweepAngle)
            {
                var size = new SizeF(xRadius, yRadius);

                var rect = new RectangleF(
                        target.X,
                        target.Y - (size.Height / 2f),
                        size.Width,
                        size.Height);

                float startAngleDegrees = startAngle * TO_DEGREES;
                float sweepAngleDegrees = sweepAngle * TO_DEGREES;

                g.DrawArc(shaftStroke, rect, startAngleDegrees, sweepAngleDegrees);

                float endPointAngle = startAngle + sweepAngle;

                var e = new Ellipse(size);

                Vector endPoint = Vector.FromPointF(e.PointOnCircumference(endPointAngle));
                Vector tangent = e.DetermineEllipseTangent(endPoint.ToPointF());

                endPoint += Vector.FromPointF(target);
                endPoint.X += (size.Width / 2f);

                g.DrawArrowHead(headStroke, headFill,
                    CreateArrowHead(
                    endPoint,
                    tangent,
                    headWidth, headLength));
            }

            public static void DrawArrowHead(
                this Graphics g,
                Pen headStroke,
                Brush headFill,
                PointF[] arrowHead)
            {
                g.FillPolygon(headFill, arrowHead);     // fill arrow head.
                g.DrawPolygon(headStroke, arrowHead);   // draw outline.
            }

            public static void DrawPoint(this Graphics g, Brush brush, PointF p, float size)
            {
                SizeF s = new SizeF(size, size);

                g.FillEllipse(brush,
                    new RectangleF(
                        p.X - s.Width / 2f,
                        p.Y - s.Height / 2f,
                        s.Width,
                        s.Height));
            }

            private static PointF[] CreateArrowHead(Vector position, Vector direction, float width, float length)
            {
                PointF[] points = new PointF[] {
                    PointF.Empty,
                    new PointF(-width / 2f, 0),
                    new PointF( 0, length),
                    new PointF( width / 2f, 0)
                };

                Matrix m = new Matrix();
                
                m.Translate(position.X, position.Y);
                m.Rotate(TO_DEGREES * (direction.Theta - (float)Math.PI / 2));
                
                m.TransformPoints(points);

                return points;
            }
        }
    }
}
