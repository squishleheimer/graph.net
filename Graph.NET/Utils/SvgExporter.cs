using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Graph.NET.Utils
{
    public class SvgExporter
    {
        public enum GraphvizExe
        {
            circo,
            dot,
            fdp,
            neato,
            osage,
            sfdp,
            twopi
        }

        #region Defaults

        public const string DEFAULT_PATH = @"C:\Program Files\Graphviz 2.28\bin";

        #endregion

        #region Delegates

        private delegate object CreationMethodDelegate(Process process);

        public delegate string PostProcessSvgMarkup(string svgMarkup);

        #endregion

        #region Static Fields

        private static string m_graphVizPath = DEFAULT_PATH;

        private static GraphvizExe m_exe = GraphvizExe.dot;

        #endregion

        #region Fields

        private string m_dotCmd = null;

        #endregion

        #region Static Properties

        public static GraphvizExe Exe
        {
            get { return m_exe; }
            set { m_exe = value; }
        }

        public static string GraphVizPath
        {
            get { return m_graphVizPath; }
            set
            {
                if (value != null)
                {
                    m_graphVizPath = value;
                }
            }
        }

        public static string DotPath
        {
            get { return Path.Combine(GraphVizPath, String.Format("{0}.exe", Exe)); }
        }

        #endregion

        /// <summary>
        /// Have a poke at the %PATH%.
        /// </summary>
        public static bool TryGetGraphvizPathAutomatically()
        {
            string path = Environment.GetEnvironmentVariable("path");

            Func<string, bool> predicate = (item) =>
            {
                return item.ToLower().Contains("graphviz");
            };

            if (!String.IsNullOrEmpty(path))
            {
                var paths = path.Split(';');

                if (paths.Any(predicate))
                {
                    path = paths.FirstOrDefault(predicate);
                }
            }

            if (Directory.Exists(path))
            {
                GraphVizPath = path;

                return true;
            }

            return false;
        }

        public SvgExporter(string dotText, bool tryGetGraphvizPathAutomatically = false)
        {
            m_dotCmd = dotText;

            if (tryGetGraphvizPathAutomatically)
            {
                TryGetGraphvizPathAutomatically();
            }
        }

        /// <summary>
        /// Writes out the m_dotCmd to a file.
        /// </summary>
        /// <param name="filePath">The output file path.</param>
        public void CreateDotFile(string filePath)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.OpenOrCreate)))
                {
                    writer.Write(m_dotCmd);
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error writing DOT file.", ex);
            }
        }

        /// <summary>
        /// Writes the svg to a file specified by the filePath.
        /// </summary>
        /// <param name="filePath">The path of the output file.</param>
        public void CreateSvgFile(string filePath, string svgCommand)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.OpenOrCreate)))
                {
                    writer.Write(svgCommand);
                }
            }
            catch (System.Exception ex)
            {
                throw new ApplicationException("Error writing SVG file.", ex);
            }
        }

        /// <summary>
        /// Creates a file of type and format indicated by the outputExtension using GraphViz
        /// installed run-times if available; otherwise exception is thrown.
        /// </summary>
        /// <param name="outputExtension">Identifies the output file type and format.</param>
        /// <param name="block">Indicates whether to make a blocking call.</param>
        public void CreateGraphFileUsingDotExecutable(string filepath, string outputExtension, bool block = false)
        {
            if (File.Exists(DotPath))
            {
                string outputFile = filepath + "." + outputExtension;

                Process process = new Process()
                {
                    StartInfo = new ProcessStartInfo(DotPath)
                    {
                        FileName = DotPath,
                        Arguments = String.Format("-T{0} -o \"{1}\"", outputExtension, outputFile),
                        UseShellExecute = false,
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        RedirectStandardInput = true
                    }
                };

                if (process.Start())
                {
                    process.StandardInput.Write(m_dotCmd);
                    process.StandardInput.Close();

                    if (block)
                    {
                        process.WaitForExit();
                    }

                    process.Close();
                }
            }
            else
            {
                throw new ApplicationException("GraphViz version not installed.");
            }
        }

        private Task<T> CreateTypeUsingDotExecutableAsync<T>(
            CancellationToken cancellationToken,
            CreationMethodDelegate creationMethod,
            bool suppressWarnings = true)
        {
            var taskCompletionSource = new TaskCompletionSource<T>();

            Process process = new Process()
            {
                EnableRaisingEvents = true,
                StartInfo = new ProcessStartInfo(DotPath)
                {
                    Arguments = @"-Tsvg" + (suppressWarnings ? @" -q" : String.Empty),
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true
                }
            };

            cancellationToken.ThrowIfCancellationRequested();

            ThreadPool.QueueUserWorkItem(_ =>
            {
                try
                {
                    if (process.Start())
                    {
                        bool isRunning = true;

                        T result = default(T);

                        Action TrySetResult = () =>
                        {
                            if (result != null && !taskCompletionSource.TrySetResult(result))
                            {
                                taskCompletionSource.TrySetException(
                                    new Exception("Result was not set!"));
                            }
                        };

                        process.Exited += (s, a) =>
                        {
                            isRunning = false;
                        };

                        cancellationToken.Register(() =>
                        {
                            if (isRunning)
                            {
                                process.Kill();
                            }

                            cancellationToken.ThrowIfCancellationRequested();
                        });

                        process.StandardInput.Write(m_dotCmd);
                        process.StandardInput.Close();

                        result = (T)creationMethod(process);

                        process.WaitForExit(); // Required for Exited handler to fire.
                        process.Close();

                        TrySetResult();
                    }
                }
                catch (System.Exception e)
                {
                    taskCompletionSource.TrySetException(
                        new Exception(String.Format("Exception starting process with path: {0}", DotPath), e));
                }
            });

            return taskCompletionSource.Task;
        }

        public Task<string> CreateStringUsingDotExecutableAsync(
            CancellationToken cancellationToken,
            PostProcessSvgMarkup ppMethod = null,
            bool suppressWarnings = true)
        {
            return CreateTypeUsingDotExecutableAsync<string>(
                cancellationToken,
                CreationMethodFactory.CreateMethod(typeof(string), cancellationToken, ppMethod),
                suppressWarnings);
        }

        public Task<ISvgType> CreateSvgTypeUsingDotExecutableAsync(
            CancellationToken cancellationToken,
            PostProcessSvgMarkup ppMethod = null,
            bool suppressWarnings = true)
        {
            return CreateTypeUsingDotExecutableAsync<ISvgType>(
                cancellationToken,
                CreationMethodFactory.CreateMethod(typeof(ISvgType), cancellationToken, ppMethod),
                suppressWarnings);
        }

        private static class CreationMethodFactory
        {
            public static CreationMethodDelegate CreateMethod(
                Type outputType, 
                CancellationToken cancellationToken, 
                PostProcessSvgMarkup ppMethod = null)
            {
                Func<Process, string> action = (process) =>
                {
                    var data = process.StandardOutput.ReadToEnd();

                    if (ppMethod != null)
                    {
                        data = ppMethod(data);
                    }

                    return data;
                };

                if (outputType == typeof(string))
                {
                    return (process) =>
                    {
                        return action(process);
                    };
                }
                else if (outputType == typeof(ISvgType))
                {
                    return (process) =>
                    {
                        return svgType.CreateSvgType(action(process));
                    };
                }

                throw new Exception("Unsupported creation type.");
            }
        }
    }
}
