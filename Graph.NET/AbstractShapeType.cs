﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Xml.Serialization;

namespace Graph.NET
{
    public abstract class AbstractShapeType : AbstractDrawable, IShapeType
    {
        /// <summary>
        /// The IGroupNode that contains this shape.
        /// <remarks>Should be assigned in Adapt implementation.</remarks>
        /// </summary>
        protected IGroupNode m_container;

        protected AbstractShapeType()
        {
            Enabled = true;
        }

        #region IShapeType Members

        [XmlIgnore]
        public bool Enabled { get; set; }

        [XmlIgnore]
        public IGroupNode Container
        {
            get { return m_container; }
        }

        public abstract void Adapt(ISvgType svg, IGroupNode container);

        public abstract void Draw(Graphics g);

        public virtual bool IntersectsPoint(PointF point)
        {
            return Rect.Contains(point);
        }

        public virtual RectangleF Bounds
        {
            get { return Rect; }
        }

        public virtual IEnumerable<ControlPoint> CreateControlPoints()
        {
            yield break;
        }

        #endregion
    }
}
