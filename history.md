#### version 1.3.0.8

 - Fixed demo transition editing logic in ControlPointMoveEnd definition in the MainForm.
 - Convert history.txt to markdown (history.md).

#### version 1.3.0.7

- "Completed" the demo by adding RemoveEdgeFromBuilder to GraphBuilder. The removal of an old transition during the handling of the ControlPointMoveEnd event.

#### version 1.3.0.6

- Perserve previous scale on graph reload.
- Refactor tidying GraphBuilder and usage in MainForm.

#### version 1.3.0.5

- Fixed the Prebuild properties.
- Improved the demo (and GraphBuilder).

#### version 1.3.0.4

- Updated GraphUtils.LoadGraph method to fix a bug with the postLoad action. It had one too many ContinueWith calls - instead of doing the postLoad action in another ContinueWith we do it all sequentially in the same ContinueWith.

#### version 1.3.0.3

- Refactored svg loading task code to be external to GraphContainerPanel. This now uses the convenience of the Task.ContinueWith method and GraphUtils now provides a LoadGraph method where generally the Svg.Init method should be called in the postLoadAction Action parameter.

#### version 1.3.0.2

- Added GraphBuilder initial version.
- Updated SvgExporter to have cooperative task cancellation of graphviz process tasks.
- Add Test Project.

#### version 1.3.0.1

- Updated build step to use .NET 4.0 xsd.exe version instead of older version that was generating a .NET 2.0 class.
- Removed useless Clear button from demo.
- Added public method to GraphContainerPanel to allow task cancellation.

#### version 1.3.0.0

- Added transition editing functionality. Transitions can be selected and then control points appear allowing modification of source/destination nodes.

#### version 1.2.0.2

- Fixed a bug so that we can run in other region like France, in which people will use ',' as the point of a float number other than '.'.

#### version 1.2.0.1

- Fixed bug that prevented top level group nodes from being selected.

#### version 1.2.0.0

- Nodes from clusters or subgraphs could not be selected. This has been fixed.
- Updated GraphPanel to be constructible without an ISvgType parameter.

#### version 1.1.0.2

- Fixed bug where paths containing points with exponential notation where not parsed correctly.
- Added some additional try-catches to SvgExporter methods.
- Removed legacy Visual Studio solution files (only VS2010 solution remains).

#### version 1.1.0.1

- Added ability to suppress warnings from dot.exe output.

#### version 1.1.0.0

- Built with VS2010.
- Targeting .NET Framework 4.0 (v4.0.30319).

#### version 1.0.0.0

- Built with VS2008.
- Targeting .NET Framework 2.0 (v2.0.50727).