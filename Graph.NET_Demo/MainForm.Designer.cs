using System.Drawing;
namespace Graph.NET_Demo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.m_splitContainer = new System.Windows.Forms.SplitContainer();
            this.m_graphContainerPanel = new Graph.NET.GraphControls.GraphContainerPanel();
            this.m_textBoxSplitContainer = new System.Windows.Forms.SplitContainer();
            this.m_DOTTextBox = new System.Windows.Forms.RichTextBox();
            this.m_verboseTextBox = new System.Windows.Forms.RichTextBox();
            this.m_statusStrip = new System.Windows.Forms.StatusStrip();
            this.m_submitToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_statusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_pathToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            this.m_pathToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.m_exeToolstripDropDown = new System.Windows.Forms.ToolStripSplitButton();
            this.m_menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.m_saveSVGAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.m_splitContainer)).BeginInit();
            this.m_splitContainer.Panel1.SuspendLayout();
            this.m_splitContainer.Panel2.SuspendLayout();
            this.m_splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_textBoxSplitContainer)).BeginInit();
            this.m_textBoxSplitContainer.Panel1.SuspendLayout();
            this.m_textBoxSplitContainer.Panel2.SuspendLayout();
            this.m_textBoxSplitContainer.SuspendLayout();
            this.m_statusStrip.SuspendLayout();
            this.m_menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // m_splitContainer
            // 
            this.m_splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_splitContainer.Location = new System.Drawing.Point(0, 24);
            this.m_splitContainer.Name = "m_splitContainer";
            this.m_splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // m_splitContainer.Panel1
            // 
            this.m_splitContainer.Panel1.Controls.Add(this.m_graphContainerPanel);
            // 
            // m_splitContainer.Panel2
            // 
            this.m_splitContainer.Panel2.Controls.Add(this.m_textBoxSplitContainer);
            this.m_splitContainer.Panel2.Controls.Add(this.m_statusStrip);
            this.m_splitContainer.Size = new System.Drawing.Size(908, 473);
            this.m_splitContainer.SplitterDistance = 298;
            this.m_splitContainer.TabIndex = 2;
            // 
            // m_graphContainerPanel
            // 
            this.m_graphContainerPanel.AllowDrop = true;
            this.m_graphContainerPanel.BackColor = System.Drawing.Color.White;
            this.m_graphContainerPanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.m_graphContainerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_graphContainerPanel.Location = new System.Drawing.Point(0, 0);
            this.m_graphContainerPanel.Name = "m_graphContainerPanel";
            this.m_graphContainerPanel.Size = new System.Drawing.Size(908, 298);
            this.m_graphContainerPanel.TabIndex = 0;
            // 
            // m_textBoxSplitContainer
            // 
            this.m_textBoxSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_textBoxSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.m_textBoxSplitContainer.Name = "m_textBoxSplitContainer";
            // 
            // m_textBoxSplitContainer.Panel1
            // 
            this.m_textBoxSplitContainer.Panel1.Controls.Add(this.m_DOTTextBox);
            // 
            // m_textBoxSplitContainer.Panel2
            // 
            this.m_textBoxSplitContainer.Panel2.Controls.Add(this.m_verboseTextBox);
            this.m_textBoxSplitContainer.Size = new System.Drawing.Size(908, 146);
            this.m_textBoxSplitContainer.SplitterDistance = 504;
            this.m_textBoxSplitContainer.TabIndex = 4;
            // 
            // m_DOTTextBox
            // 
            this.m_DOTTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_DOTTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_DOTTextBox.Location = new System.Drawing.Point(0, 0);
            this.m_DOTTextBox.Name = "m_DOTTextBox";
            this.m_DOTTextBox.Size = new System.Drawing.Size(504, 146);
            this.m_DOTTextBox.TabIndex = 1;
            this.m_DOTTextBox.Text = "";
            this.m_DOTTextBox.TextChanged += new System.EventHandler(this.m_DOTTextBox_TextChanged);
            // 
            // m_verboseTextBox
            // 
            this.m_verboseTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.m_verboseTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_verboseTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_verboseTextBox.ForeColor = System.Drawing.SystemColors.Desktop;
            this.m_verboseTextBox.Location = new System.Drawing.Point(0, 0);
            this.m_verboseTextBox.Name = "m_verboseTextBox";
            this.m_verboseTextBox.ReadOnly = true;
            this.m_verboseTextBox.Size = new System.Drawing.Size(400, 146);
            this.m_verboseTextBox.TabIndex = 3;
            this.m_verboseTextBox.Text = "";
            // 
            // m_statusStrip
            // 
            this.m_statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_submitToolStripStatusLabel,
            this.m_statusToolStripStatusLabel,
            this.m_pathToolStripSplitButton,
            this.m_exeToolstripDropDown});
            this.m_statusStrip.Location = new System.Drawing.Point(0, 146);
            this.m_statusStrip.Name = "m_statusStrip";
            this.m_statusStrip.Size = new System.Drawing.Size(908, 25);
            this.m_statusStrip.TabIndex = 2;
            this.m_statusStrip.Text = "statusStrip1";
            // 
            // m_submitToolStripStatusLabel
            // 
            this.m_submitToolStripStatusLabel.BackColor = System.Drawing.SystemColors.Control;
            this.m_submitToolStripStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.m_submitToolStripStatusLabel.BorderStyle = System.Windows.Forms.Border3DStyle.RaisedInner;
            this.m_submitToolStripStatusLabel.ForeColor = System.Drawing.Color.Black;
            this.m_submitToolStripStatusLabel.Image = ((System.Drawing.Image)(resources.GetObject("m_submitToolStripStatusLabel.Image")));
            this.m_submitToolStripStatusLabel.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.m_submitToolStripStatusLabel.Name = "m_submitToolStripStatusLabel";
            this.m_submitToolStripStatusLabel.Size = new System.Drawing.Size(65, 20);
            this.m_submitToolStripStatusLabel.Text = "Submit";
            this.m_submitToolStripStatusLabel.Click += new System.EventHandler(this.submitToolStripStatusLabel_Click);
            this.m_submitToolStripStatusLabel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_submitToolStripStatusLabel_MouseDown);
            this.m_submitToolStripStatusLabel.MouseEnter += new System.EventHandler(this.m_submitToolStripStatusLabel_MouseEnter);
            this.m_submitToolStripStatusLabel.MouseLeave += new System.EventHandler(this.m_submitToolStripStatusLabel_MouseLeave);
            this.m_submitToolStripStatusLabel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.m_submitToolStripStatusLabel_MouseUp);
            // 
            // m_statusToolStripStatusLabel
            // 
            this.m_statusToolStripStatusLabel.BackColor = System.Drawing.SystemColors.Control;
            this.m_statusToolStripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.m_statusToolStripStatusLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.m_statusToolStripStatusLabel.Name = "m_statusToolStripStatusLabel";
            this.m_statusToolStripStatusLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.m_statusToolStripStatusLabel.Size = new System.Drawing.Size(759, 20);
            this.m_statusToolStripStatusLabel.Spring = true;
            this.m_statusToolStripStatusLabel.Text = "Drag And Drop or Submit DOT Text";
            this.m_statusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // m_pathToolStripSplitButton
            // 
            this.m_pathToolStripSplitButton.BackColor = System.Drawing.SystemColors.Control;
            this.m_pathToolStripSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_pathToolStripSplitButton.DropDownButtonWidth = 16;
            this.m_pathToolStripSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_pathToolStripTextBox});
            this.m_pathToolStripSplitButton.Image = ((System.Drawing.Image)(resources.GetObject("m_pathToolStripSplitButton.Image")));
            this.m_pathToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_pathToolStripSplitButton.Margin = new System.Windows.Forms.Padding(0);
            this.m_pathToolStripSplitButton.Name = "m_pathToolStripSplitButton";
            this.m_pathToolStripSplitButton.Size = new System.Drawing.Size(37, 25);
            this.m_pathToolStripSplitButton.Text = "graphviz path";
            this.m_pathToolStripSplitButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.m_pathToolStripSplitButton.ButtonClick += new System.EventHandler(this.m_pathToolStripSplitButton_ButtonClick);
            // 
            // m_pathToolStripTextBox
            // 
            this.m_pathToolStripTextBox.AutoToolTip = true;
            this.m_pathToolStripTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.m_pathToolStripTextBox.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_pathToolStripTextBox.Margin = new System.Windows.Forms.Padding(0);
            this.m_pathToolStripTextBox.Name = "m_pathToolStripTextBox";
            this.m_pathToolStripTextBox.Size = new System.Drawing.Size(256, 21);
            this.m_pathToolStripTextBox.ToolTipText = "graphviz path";
            this.m_pathToolStripTextBox.TextChanged += new System.EventHandler(this.m_pathToolStripTextBox_TextChanged);
            // 
            // m_exeToolstripDropDown
            // 
            this.m_exeToolstripDropDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.m_exeToolstripDropDown.Image = ((System.Drawing.Image)(resources.GetObject("m_exeToolstripDropDown.Image")));
            this.m_exeToolstripDropDown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.m_exeToolstripDropDown.Name = "m_exeToolstripDropDown";
            this.m_exeToolstripDropDown.Size = new System.Drawing.Size(32, 23);
            this.m_exeToolstripDropDown.Text = "m_exeToolstripDropDown";
            // 
            // m_menuStrip
            // 
            this.m_menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.m_menuStrip.Location = new System.Drawing.Point(0, 0);
            this.m_menuStrip.Name = "m_menuStrip";
            this.m_menuStrip.Size = new System.Drawing.Size(908, 24);
            this.m_menuStrip.TabIndex = 3;
            this.m_menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_saveSVGAsToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // m_saveSVGAsToolStripMenuItem
            // 
            this.m_saveSVGAsToolStripMenuItem.Name = "m_saveSVGAsToolStripMenuItem";
            this.m_saveSVGAsToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.m_saveSVGAsToolStripMenuItem.Text = "Save graph as ...";
            this.m_saveSVGAsToolStripMenuItem.Click += new System.EventHandler(this.m_saveSVGAsToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.exportToolStripMenuItem.Text = "Export markup as ...";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(908, 497);
            this.Controls.Add(this.m_splitContainer);
            this.Controls.Add(this.m_menuStrip);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.m_menuStrip;
            this.Name = "MainForm";
            this.Text = "Graph.NET Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.m_splitContainer.Panel1.ResumeLayout(false);
            this.m_splitContainer.Panel2.ResumeLayout(false);
            this.m_splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_splitContainer)).EndInit();
            this.m_splitContainer.ResumeLayout(false);
            this.m_textBoxSplitContainer.Panel1.ResumeLayout(false);
            this.m_textBoxSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.m_textBoxSplitContainer)).EndInit();
            this.m_textBoxSplitContainer.ResumeLayout(false);
            this.m_statusStrip.ResumeLayout(false);
            this.m_statusStrip.PerformLayout();
            this.m_menuStrip.ResumeLayout(false);
            this.m_menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer m_splitContainer;
        private System.Windows.Forms.RichTextBox m_DOTTextBox;
        private System.Windows.Forms.StatusStrip m_statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel m_submitToolStripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel m_statusToolStripStatusLabel;
        private System.Windows.Forms.ToolStripSplitButton m_pathToolStripSplitButton;
        private System.Windows.Forms.ToolStripTextBox m_pathToolStripTextBox;
        private Graph.NET.GraphControls.GraphContainerPanel m_graphContainerPanel;
        private System.Windows.Forms.MenuStrip m_menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem m_saveSVGAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton m_exeToolstripDropDown;
        private System.Windows.Forms.RichTextBox m_verboseTextBox;
        private System.Windows.Forms.SplitContainer m_textBoxSplitContainer;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;


    }
}

