using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Graph.NET;
using Graph.NET.GraphControls;
using Graph.NET.Utils;
using Graph.NET.Utils.GraphicsExtensions;
using System.Threading;
using System.Text;

namespace Graph.NET_Demo
{
    public partial class MainForm : Form
    {
        const string DEFAULT_DOT = "digraph myGraph {\n    \"Graph\"->\".\"->\".\"->\"NET\"->\".\"->\"Graph\"\n}";

        public MainForm()
        {
            InitializeComponent();

            m_graphContainerPanel.DragEnter += new DragEventHandler(m_graphContainerPanel_DragEnter);
            m_graphContainerPanel.DragDrop += new DragEventHandler(m_graphContainerPanel_DragDrop);

            //m_graphContainerPanel.GraphPanel.PostPaint += new PaintEventHandler(GraphPanel_PostPaint);

            this.MouseWheel += new MouseEventHandler(m_graphContainerPanel.GraphPanel_MouseWheel);
            this.KeyDown += new KeyEventHandler(OnKeyDown);

            // Set all un-handled task exceptions to observed. This is
            // necessary to enable catching TPL thread exceptions. 
            TaskScheduler.UnobservedTaskException += (sender, args) => { args.SetObserved(); };

            Action<ToolStripItem> HighlightAction = (selectedToolStripItem) =>
            {
                m_exeToolstripDropDown.DropDownItems.Cast<ToolStripItem>().ToList().ForEach((t) =>
                {
                    t.ForeColor = (selectedToolStripItem == t) ? SystemColors.Highlight : Color.Black;
                });
            };

            Enum.GetNames(typeof(SvgExporter.GraphvizExe)).ToList().ForEach((item) =>
            {
                var x = m_exeToolstripDropDown.DropDownItems.Add(
                    item.ToString(),
                    null,
                    (s, a) =>
                    {
                        HighlightAction(s as ToolStripItem);

                        SvgExporter.Exe = (SvgExporter.GraphvizExe)Enum.Parse(typeof(SvgExporter.GraphvizExe), item.ToString());

                        if (!String.IsNullOrWhiteSpace(m_verboseTextBox.Text))
                        {
                            LoadDot(m_verboseTextBox.Text);
                        }
                    });

                if (item == SvgExporter.Exe.ToString())
                {
                    HighlightAction(x);
                }
            });

            if (SvgExporter.TryGetGraphvizPathAutomatically())
            {
                m_pathToolStripTextBox.Text = SvgExporter.GraphVizPath;
            }
            else
            {
                if (Directory.Exists(Graph.NET_Demo.Properties.Settings.Default.GVPath))
                {
                    m_pathToolStripTextBox.Text = Graph.NET_Demo.Properties.Settings.Default.GVPath;
                }
            }            

            m_graphContainerPanel.GraphPanel.TransitionAdded += (s, a) =>
            {
                var builder = new GraphBuilder(m_graphContainerPanel.Svg);

                var sb = builder.BeginBuilder();

                string destId = String.Empty;

                if (a.Destination == null)
                {
                    sb = builder.AddNodeToBuilder(sb, out destId);
                }
                else
                {
                    destId = a.Destination.Id;
                }

                if (!String.IsNullOrWhiteSpace(destId))
                {
                    sb = builder.AddEdgeToBuilder(sb, out destId, String.Empty, a.Source.Id, destId);
                }

                LoadDot(builder.EndBuilder(sb));
            };

            m_graphContainerPanel.GraphPanel.ControlPointMoveEnd += (s, a) =>
            {
                if (a.Target != null)
                {
                    var builder = new GraphBuilder(m_graphContainerPanel.Svg);

                    var sb = builder.BeginBuilder();

                    string edgeId = a.ControlPoint.Owner.Container.Id;

                    var edge = m_graphContainerPanel.Svg.GetGnodeById(edgeId);
                    
                    var src = GraphUtils.GetSourceNode(m_graphContainerPanel.Svg, edge).FirstOrDefault();
                    var dst = GraphUtils.GetDestinationNode(m_graphContainerPanel.Svg, edge).FirstOrDefault();

                    if (src != null && dst != null)
                    {
                        sb = builder.RemoveEdgeFromBuilder(sb, edgeId, String.Empty, src.Id, dst.Id);

                        var newSrcId = (a.ControlPoint.Type == ControlPoint.ControlPointType.head) ? src.Id : a.Target.Id;
                        var newDestId = (a.ControlPoint.Type == ControlPoint.ControlPointType.tail) ? dst.Id : a.Target.Id;

                        string newEdgeId = String.Empty;
                        sb = builder.AddEdgeToBuilder(sb, out newEdgeId, String.Empty, newSrcId, newDestId);
                    }

                    LoadDot(builder.EndBuilder(sb));
                }
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            m_DOTTextBox.Text = DEFAULT_DOT;

            Icon = global::Graph.NET_Demo.Properties.Resources.graph_32x32;

            if (Directory.Exists(SvgExporter.GraphVizPath))
            {
                LoadDot(DEFAULT_DOT);
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    if (m_graphContainerPanel.GraphPanel.SvgLoaded)
                    {
                        m_graphContainerPanel.GraphPanel.CancelEdit();
                        m_graphContainerPanel.GraphPanel.ClearSelectedNode();
                    }
                    CancelLoad();
                    break;
                case Keys.S:
                    if (e.Control && m_graphContainerPanel.GraphPanel.SvgLoaded)
                    {
                        string filePath =
                            Path.Combine(
                            Application.StartupPath,
                            String.Format("{0}.svg", m_graphContainerPanel.Svg.Title));

                        svgType.Save(filePath, m_graphContainerPanel.Svg);
                    }
                    break;
                case Keys.G:
                    if (e.Control && !String.IsNullOrWhiteSpace(m_DOTTextBox.Text))
                    {
                        string filePath =
                            Path.Combine(
                            Application.StartupPath,
                            String.Format("{0}.gv", "myGraph"));

                        File.WriteAllText(filePath, m_DOTTextBox.Text);
                    }
                    break;
            }
        }

        CancellationTokenSource m_source = null;

        /// <summary>
        /// Will halt any currently running loading tasks.
        /// </summary>
        public void CancelLoad()
        {
            if (m_source != null)
            {
                try
                {
                    m_source.Cancel();
                }
                catch (AggregateException ae)
                {
                    var squish = ae.Flatten();

                    // Mark all exceptions as handled.
                    squish.Handle(e =>
                    {
                        if (e is OperationCanceledException)
                        {
                            m_statusToolStripStatusLabel.Text = "Cancelled";
                        }

                        return true;
                    });
                }
            }
        }

        private void GraphPanel_PostPaint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;

            PointF point = m_graphContainerPanel.GraphPanel.ForeNodeCentre;

            if (!point.IsEmpty)
            {
                e.Graphics.DrawPoint(Brushes.Red, point, 5f);
            }
        }

        private void m_graphContainerPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void m_graphContainerPanel_DragDrop(object sender, DragEventArgs e)
        {
            var filename = ((string[])e.Data.GetData(DataFormats.FileDrop)).FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(filename) && 
                File.Exists(filename) && 
                Path.HasExtension(filename) && 
                Path.GetExtension(filename).ToLowerInvariant() == ".svg")
            {
                // Blank the canvas.
                m_graphContainerPanel.Svg = null;
                m_statusToolStripStatusLabel.Text = "Loading ...";

                m_graphContainerPanel.Svg = svgType.Load(filename).Init(AppDomain.CurrentDomain.BaseDirectory);

                m_statusToolStripStatusLabel.Text = "OK";
            }
        }

        private void LoadDot(string dot)
        {
            m_source = new CancellationTokenSource();

            var scale = m_graphContainerPanel.GraphPanel.Scale;

            // Blank the canvas.
            m_graphContainerPanel.Svg = null;
            m_statusToolStripStatusLabel.Text = "Loading ...";

            GraphUtils.LoadGraph(dot, m_graphContainerPanel, m_source, null,
                () =>
                {
                    m_verboseTextBox.Text = new GraphBuilder(
                        m_graphContainerPanel.Svg.Init(AppDomain.CurrentDomain.BaseDirectory)).ToString();

                    m_statusToolStripStatusLabel.Text = "OK";

                    m_graphContainerPanel.GraphPanel.Scale = scale;

                    m_source = null;
                });
        }

        private void submitToolStripStatusLabel_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(m_DOTTextBox.Text))
            {
                if (Directory.Exists(SvgExporter.GraphVizPath))
                {
                    LoadDot(m_DOTTextBox.Text);
                }
                else
                {
                    m_statusToolStripStatusLabel.Text =
                        String.Format("Couldn't run dot.exe from path: {0}", SvgExporter.GraphVizPath);
                }
            }
            else
            {
                m_statusToolStripStatusLabel.Text = String.Format("No text");
            }
        }

        private void m_pathToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            CheckPath(m_pathToolStripTextBox.Text);
        }

        private void m_pathToolStripSplitButton_ButtonClick(object sender, EventArgs e)
        {
            CheckPath(m_pathToolStripTextBox.Text);
        }

        private void CheckPath(string path)
        {
            if (Directory.Exists(path))
            {
                SvgExporter.GraphVizPath = path;

                m_statusToolStripStatusLabel.Text = String.Format("Ready");
            }
            else
            {
                m_statusToolStripStatusLabel.Text =
                    String.Format("Path does not exist: {0}", path);
            }
        }

        private void m_submitToolStripStatusLabel_MouseDown(object sender, MouseEventArgs e)
        {
            m_submitToolStripStatusLabel.BorderStyle = Border3DStyle.SunkenInner;
        }

        private void m_submitToolStripStatusLabel_MouseUp(object sender, MouseEventArgs e)
        {
            m_submitToolStripStatusLabel.BorderStyle = Border3DStyle.RaisedInner;
        }

        private void m_submitToolStripStatusLabel_MouseEnter(object sender, EventArgs e)
        {
            m_submitToolStripStatusLabel.ForeColor = Color.Blue;
        }

        private void m_submitToolStripStatusLabel_MouseLeave(object sender, EventArgs e)
        {
            m_submitToolStripStatusLabel.ForeColor = Color.Black;
        }

        private void m_DOTTextBox_TextChanged(object sender, EventArgs e)
        {
            m_submitToolStripStatusLabel.Enabled = !String.IsNullOrWhiteSpace(m_DOTTextBox.Text);
        }

        private void m_saveSVGAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog()
            {
                DefaultExt = ".svg",
                Filter = @"SVG Files(*.xml;*.svg)|*.xml;*.svg|Graphviz Files(*.gv;*.dot;)|*.gv;*.dot",
                RestoreDirectory = true,
                InitialDirectory = Application.StartupPath
            };

            if (dialog.ShowDialog() == DialogResult.OK && dialog.CheckPathExists)
            {
                SaveGraphAs(dialog.FileName);
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog()
            {
                DefaultExt = ".svg",
                Filter = @"SVG Files(*.xml;*.svg)|*.xml;*.svg|Graphviz Files(*.gv;*.dot;)|*.gv;*.dot",
                RestoreDirectory = true,
                InitialDirectory = Application.StartupPath
            };

            if (dialog.ShowDialog() == DialogResult.OK && dialog.CheckPathExists)
            {
                ExportMarkupAs(dialog.FileName);
            }
        }

        private void SaveGraphAs(string filePath)
        {
            if (Path.HasExtension(filePath))
            {
                string ext = Path.GetExtension(filePath).ToLowerInvariant();

                if (ext == ".xml" || ext == ".svg")
                {
                    m_graphContainerPanel.GraphPanel.SaveGraphAs(filePath);
                }
                else if (ext == ".gv" || ext == ".dot")
                {
                    File.WriteAllText(filePath, m_DOTTextBox.Text);
                }
            }
        }

        private void ExportMarkupAs(string filePath)
        {
            if (!String.IsNullOrWhiteSpace(m_DOTTextBox.Text) && Path.HasExtension(filePath))
            {
                string ext = Path.GetExtension(filePath).ToLowerInvariant();

                new SvgExporter(m_DOTTextBox.Text).CreateGraphFileUsingDotExecutable(filePath, ext.TrimStart('.'));
            }
        }
    }
}